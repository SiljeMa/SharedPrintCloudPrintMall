<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- 兼容浏览器 -->
<meta http-equiv="X-UA-Compatiable" content="IE=edge">
<!-- 屏幕自适应 -->
<meta name="viewport" content="width=device-width,initial-scale=1" >
<title>云打印商城首页</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin.css?version=4">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css?version=3">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.datagrid.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.uploader.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.4.1.js"></script>
 	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.datagrid.js?version=12"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.chart.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js"></script>
<body >
	 <div class="guide">
		<nav class="navbar navbar-default" role="navigation">
		    <div class="collapse navbar-collapse navbar-collapse-example">
		      <ul class="nav navbar-nav nav-justified">
		      	<li class="dropdown" id="userTask">
          		<!-- <a href="your/nice/url" class="dropdown-toggle" data-toggle="dropdown"><font size="3" color="blue">欢迎您</font><b class="caret"></b></a> -->
         		<ul class="dropdown-menu" role="menu">
         			<li id="register"><a><font size="2">注册用户</font></a></li>
           			<li id="updatePwd"><a><font size="2">修改密码</font></a></li>
           			<li id="updateInfo"><a><font size="2">修改资料</font></a></li>
           			<li id="loginOut"><a><font size="2">退出登录</font></a></li>
         		</ul>
        		</li>
        		 <li><a><font size="3">首页</font></a></li>
				<li><a href="${pageContext.request.contextPath}/admin/order.jsp"><font size="3">订单</font></a></li>
		       <li><a href="${pageContext.request.contextPath}/admin/shopCard.jsp"><font size="3">购物车</font></a></li>
		        <li><a href="${pageContext.request.contextPath}/admin/file.jsp"><font size="3">文件中心</font></a></li>
		          <li><a href="${pageContext.request.contextPath}/admin/integral.jsp"><font size="3">积分中心</font></a></li>
		            <li><a href="${pageContext.request.contextPath}/admin/integralcontent.jsp"><font size="3">积分明细</font></a></li>
		      </ul>
	
		    </div>
		</nav>
	</div>
	
	<div id="header">
		<div id="nav">
			<div class="panel panel-primary">
 			 <div id="myNiceCarousel" class="carousel slide" data-ride="carousel">
			  <!-- 圆点指示器 -->
			  <ol class="carousel-indicators">
			    <li data-target="#myNiceCarousel" data-slide-to="0" class="active"></li>
			    <li data-target="#myNiceCarousel" data-slide-to="1"></li>
			    <li data-target="#myNiceCarousel" data-slide-to="2"></li>
			  </ol>
			
			  <!-- 广告区 -->
			  <!-- 轮播项目 -->
			  <div class="carousel-inner">
			    <div class="item active">
			      <img alt="First slide" src="${pageContext.request.contextPath}/images/ads1.jpg">
			      <div class="carousel-caption">
			      </div>
			    </div>
			    <div class="item">
			      <img alt="Second slide" src="${pageContext.request.contextPath}/images/ads2.jpg">
			      <div class="carousel-caption">
			      </div>
			    </div>
			    <div class="item">
			      <img alt="Third slide" src="${pageContext.request.contextPath}/images/ads3.jpg">
			      <div class="carousel-caption">
			      </div>
			    </div>
			  </div>
			
			  <!-- 项目切换按钮 -->
			  <a class="left carousel-control" href="#myNiceCarousel" data-slide="prev">
			    <span class="icon icon-chevron-left"></span>
			  </a>
			  <a class="right carousel-control" href="#myNiceCarousel" data-slide="next">
			    <span class="icon icon-chevron-right"></span>
			  </a>
			</div>
			</div>
		</div>
	</div>
	
	<div id="search_area">
		<div class="panel panel-primary">
  				<ul class="nav nav-primary nav-stacked">
		  		<div class="panel">
  				<div class="panel-heading">
   				商品搜索
 				</div>
  				<div class="panel-body">
    				<ul>
    				<li>&nbsp;商品名称</li>
    				<div class="input-group">
					  <input id="queryGoodsNameLike" name="queryGoodsNameLike" type="text" class="form-control">
					  <span class="input-group-btn">
					    <button id="searchBtn" class="btn btn-default" type="button">搜索</button>
					  </span>
					</div>
    				</ul>
  				</div>
				</div>
				</ul>
		</div>
	</div>
	
	<div id="container">
		<div class="sidebar">
		<div class="panel panel-primary">
  			<ul class="nav nav-primary nav-stacked" id="CatogoryList">
		  		<li class="active" value=""><a>商品类别</a></li>
			</ul> 
		</div>
		</div>
		<div class="main">
			<div class="panel panel-primary">
  				<div class="panel">
  				<div class="panel-heading">
   					商品列表
 				</div>
  				<div class="panel-body">
  					<!-- 品牌头，DIV中放置品牌 -->
  					<div class="brand_area">
  						<ul class="nav nav-secondary" id="BrandList">
						 <li id="" class="active"><a>品牌</a></li>
						 	
						</ul>
  					</div>
  					<!-- 商品展示区域 -->
  					<div class="row" id="MainBox" style="height: 830px;">
  							
  					</div>
  					
  				</div>
				</div>
			  <!-- 分页 -->
 			 <div  class="pager" id="goodsPager" align="right" ></div>
			</div>
		</div>
	</div>
	</div>
	
	<!-- 商品详情显示界面 -->
<div class="modal fade" id="detailModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">商品详情</h4>
      </div>
      <form id="detailForm">
      <div class="modal-body">
        <div class="row" style="margin: 20px; width: 200px; height: 200px;">
	 			<div id="myNiceCarousel" class="carousel slide" data-ride="carousel">
			  <!-- 圆点指示器 -->
			  <ol class="carousel-indicators" id="pointList">
			   <!--  <li data-target="#myNiceCarousel" data-slide-to="0" class="active"></li>
			    <li data-target="#myNiceCarousel" data-slide-to="1"></li>
			    <li data-target="#myNiceCarousel" data-slide-to="2"></li> -->
			  </ol>
			
			  <!-- 广告区 -->
			  <!-- 商品图片 -->
			  <div class="carousel-inner" id="imgList">
			    
			  </div>
			</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">商品名称</span>
					  <span class="form-control"  id="detailGoodsName"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">商品价格</span>
				  <span class="form-control"  id="detailGoodsPrice"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">品牌</span>
					  <span class="form-control"  id="detailBrand"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">类别</span>
				  <span class="form-control"  id="detailCatogory"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					  <span class="input-group-addon">商品描述</span>
				  	  <span class="form-control"  id="detailDescription"></span>
				</div>
	 		</div>
	 	</div>
      </div>
      <div class="modal-footer">
      	<input type="hidden" id="goodsId" name="goodsId" />
      	<button type="button" class="btn btn-default " data-dismiss="modal" id="addShopCard" value="">加入购物车</button>
        <button type="button" class="btn btn-default " data-dismiss="modal">返回</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- 用户注册页面  -->
<div class="modal fade" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">用户注册</h4>
      </div>
      <form id="registerForm">
      <div class="modal-body">
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">姓名<font color="red">*</font></span>
			 	  <input type="text" class="form-control" id="addTrueName" name="addTrueName">
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">用户名<font color="red">*</font></span>
					  <input type="text" class="form-control" id="addLoginName" name="addLoginName">
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					  <span class="input-group-addon">所属机构<font color="red">*</font></span>
					  <select class="form-control" id="addUserOrgCode" name="addUserOrgCode">
					    <option value="">请选择</option>
					  </select>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	<div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">所属地区<font color="red">*</font></span>
			 	  <span class="input-group-addon"></span>
					  <select class="form-control" id="addProvince" name="addProvince" 
					  style="width: 160px;">
					    <option value="">省</option>
				  	  </select>
				  <span class="input-group-addon"></span>
				  	  <select class="form-control" id="addCity" name="addCity" style="width: 160px;">
					    <option value="">市</option>
				  	  </select>
				  <span class="input-group-addon"></span>
				  	  <select class="form-control" id="addRegion" name="addRegion" style="width: 160px;">
					    <option value="">区</option>
				  	  </select>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	<div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">所属角色<font color="red">*</font></span>
					  <select class="form-control" id="addUserRoleCode" name="addUserRoleCode">
					    <option value="">请选择</option>
					  </select>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">性别<font color="red">*</font></span>
					  <select class="form-control" id="addUserSex" name="addUserSex">
					    <option value="0">请选择</option>
					  </select>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	   <span class="input-group-addon">手机号码<font color="red">*</font></span>
			 	   <input type="text" class="form-control" id="addMobile" name="addMobile" >
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					 <span class="input-group-addon">邮箱<font color="red">*</font></span>
			 	     <input type="text" class="form-control" id="addEmail" name="addEmail" >
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	<div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	   <span class="input-group-addon">QQ<font color="red">*</font></span>
			 	   <input type="text" class="form-control" id="addQq" name="addQq" >
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					 <span class="input-group-addon">微信<font color="red">*</font></span>
			 	     <input type="text" class="form-control" id="addWeixin" name="addWeixin" >
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	 <div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					 <span class="input-group-addon">地址<font color="red">*</font></span>
			 	     <textarea rows="5" cols="60" class="form-control"
					  id="addAddress" name="addAddress"></textarea>
				</div>
	 		</div>
	 	</div>
      </div>
      <div class="modal-footer">
      	<input type="hidden" id="addAreaCode" name="addAreaCode" />
        <button type="button" id="cancelBtn" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary"  id="addSaveBtn">保存</button>
      </div>
       </form>
    </div>
  </div>
</div>

<!-- 资料修改页面  -->
<div class="modal fade" id="updateModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">修改机构信息</h4>
      </div>
      <form id="updateForm">
      <div class="modal-body">
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">姓名<font color="red">*</font></span>
			 	  <input type="text" class="form-control" id="updateTrueName" name="updateTrueName">
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">用户名<font color="red">*</font></span>
					  <input type="text" class="form-control" id="updateLoginName" name="updateLoginName">
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					  <span class="input-group-addon">所属机构<font color="red">*</font></span>
					  <select class="form-control" id="updateUserOrgCode" name="updateUserOrgCode">
					    <option value="">请选择</option>
					  </select>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	<div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">所属地区<font color="red">*</font></span>
			 	  <span class="input-group-addon"></span>
					  <select class="form-control" id="updateProvince" name="updateProvince" 
					  style="width: 160px;">
					    <option value="">省</option>
				  	  </select>
				  <span class="input-group-addon"></span>
				  	  <select class="form-control" id="updateCity" name="updateCity" style="width: 160px;">
					    <option value="">市</option>
				  	  </select>
				  <span class="input-group-addon"></span>
				  	  <select class="form-control" id="updateRegion" name="updateRegion" style="width: 160px;">
					    <option value="">区</option>
				  	  </select>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	<div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">所属角色<font color="red">*</font></span>
					  <select class="form-control" id="updateUserRoleCode" name="updateUserRoleCode">
					    <option value="">请选择</option>
					  </select>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">性别<font color="red">*</font></span>
					  <select class="form-control" id="updateUserSex" name="updateUserSex">
					    <option value="0">请选择</option>
					  </select>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	   <span class="input-group-addon">手机号码<font color="red">*</font></span>
			 	   <input type="text" class="form-control" id="updateMobile" name="updateMobile" >
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					 <span class="input-group-addon">邮箱<font color="red">*</font></span>
			 	     <input type="text" class="form-control" id="updateEmail" name="updateEmail" >
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	<div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	   <span class="input-group-addon">QQ<font color="red">*</font></span>
			 	   <input type="text" class="form-control" id="updateQq" name="updateQq" >
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					 <span class="input-group-addon">微信<font color="red">*</font></span>
			 	     <input type="text" class="form-control" id="updateWeixin" name="updateWeixin" >
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	 <div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					 <span class="input-group-addon">地址<font color="red">*</font></span>
			 	     <textarea rows="5" cols="60" class="form-control"
					  id="updateAddress" name="updateAddress"></textarea>
				</div>
	 		</div>
	 	</div>
      </div>
      <div class="modal-footer">
      	<!-- 把旧的值传到后台 -->
      	<input type="hidden" id="updateUserId" name="updateUserId" />
        <button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary"  id="updateSave">保存</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- 密码修改界面 -->
<div class="modal fade" id="updatePwdModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">修改密码</h4>
      </div>
      <form id="updatePwdForm">
      	<div class="modal-body">
	        <div class="row">
		 		<div class="col-md-12">
		 			<div class="input-group ">
				 	  <span class="input-group-addon">初始密码<font color="red">*</font></span>
				 	  <input type="text" class="form-control" id="initPwd" name="initPwd">
					</div>
		 		</div>
		 	</div>
		 	<div style="margin-top:10px;"></div>
		 	<div class="row">
		 		<div class="col-md-12">
		 			<div class="input-group ">
				 	  <span class="input-group-addon">新密码<font color="red">*</font></span>
				 	  <input type="password" class="form-control" id="newPwd" name="newPwd">
					</div>
		 		</div>
		 	</div>
		 	<div style="margin-top:10px;"></div>
		 	<div class="row">
		 		<div class="col-md-12">
		 			<div class="input-group ">
				 	  <span class="input-group-addon">再次输入新密码<font color="red">*</font></span>
				 	  <input type="password" class="form-control" id="reInputPwd" name="reInputPwd">
					</div>
		 		</div>
		 	</div>
		 </div>
      <div class="modal-footer">
      	<!-- 把旧的值传到后台 -->
        <button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary"  id="updatePwdSave">确定</button>
      </div>
      </form>
    </div>
  </div>
</div>
	
	
   	<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}"/>
   	<script type="text/javascript" src="${pageContext.request.contextPath}/js/admin.js?version=17"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/authority.js?version=1"></script>

</body>
</html>