<%@ page language="java"  pageEncoding="UTF-8"%>
<div class="container-fluid">
	<div class="row">
		 <div class="col-md-12">
	  		<a href="${pageContext.request.contextPath}/admin/admin.jsp">首页</a>
	 	 </div>
	</div>
 	<div  class="row">
		 <div class="col-md-12">
	  		<canvas id="myLineChart" width="1250" height="400"></canvas>
	 	 </div>
	</div>
	<div class="row">
		 <div class="col-md-12">
	  		<canvas id="myBarChart" width="1250" height="400"></canvas>
	 	 </div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/adminMainRight.js"></script>