<%@ page language="java"  pageEncoding="UTF-8"%>
<body>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.chart.js"></script>
<div class="container-fluid">
  <div class="row">
  	<div class="col-md-12">
  		<a href="javascript:;"  style="color:#0288d1"><i class="icon icon-home"></i></a>
  		&nbsp;/&nbsp;首页
  	</div>
  </div>
  <div class="row">
  	<div class="col-md-12">
  		<canvas id="myLineChart" width="1054" height="379" style="width: 1054px; height: 379px;"></canvas>
  	</div>
  </div>
  <div class="row">
  	<div class="col-md-12">
  		<canvas id="myBarChart" width="1054" height="379" style="width: 1054px; height: 379px;"></canvas>
  	</div>
  </div>
</div>
<script>
var data = {
	    // labels 数据包含依次在X轴上显示的文本标签
	    labels: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
	    datasets: [{
	        // 数据集名称，会在图例中显示
	        label: "红队",
	        color: "red",
	        // 数据集
	        data: [65, 59, 80, 81, 56, 55, 40, 44, 55, 70, 30, 40]
	    }, {
	        label: "绿队",
	        color: "green",
	        data: [28, 48, 40, 19, 86, 27, 90, 60, 30, 44, 50, 66]
	    }]
	};

	var options = {}; // 图表配置项，可以留空来使用默认的配置

	var myLineChart = $("#myLineChart").lineChart(data, options);
	
	var data2 = {
		    labels: ["一月", "二月", "三月", "四月", "五月", "六月", "七月"],
		    datasets: [
		        {
		            label: "蓝队",
		            color: 'blue',
		            data: [65, 59, 80, 81, 56, 55, 40]
		        }, {
		            label: "绿队",
		            color: 'green',
		            data: [28, 48, 40, 19, 86, 27, 90]
		        }
		    ]
		};

		var options2 = {responsive: true}; // 图表配置项，可以留空来使用默认的配置
		var myBarChart = $('#myBarChart').barChart(data2, options2);
</script>
</body>
	
