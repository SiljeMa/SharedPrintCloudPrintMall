<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- 兼容浏览器 -->
<meta http-equiv="X-UA-Compatiable" content="IE=edge">
<!-- 屏幕自适应 -->
<meta name="viewport" content="width=device-width,initial-scale=1" >
<title>云打印商城首页</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin.css?version=4">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css?version=3">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.datagrid.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.uploader.css">
	
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.4.1.js"></script>
 	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>

	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.datagrid.js?version=12"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.chart.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js"></script>
<body >
	

<div class="panel ">
	 <div class="panel-heading">
	   	查询区域
	 </div>
	 <div class="panel-body row">
	 	<div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">文件名称</span>
					  <input type="text" class="form-control" id="queryFileName" name="queryFileName">
				</div>
	 		</div>
	 	
	 		<div class="col-md-6">
			    <button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
	 		</div>
	 	</div>
	 </div>
 </div>
 <div class="datagrid">
  <div class="search-box ">
  	<button class="btn btn-success " type="button"
  	  data-toggle="modal" data-target="#addModal" id="addBtn"><i class="icon-plus"></i>新增</button>
  	  &nbsp;&nbsp;&nbsp;&nbsp;
  	
    <button class="btn btn-danger " type="button" id="deleteBtn"><i class="icon icon-trash"></i>删除</button>
    	&nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn " type="button" id="detailBtn"><i class="icon icon-info"></i>详情</button>
  </div>
  <div id="listDataGrid" class="datagrid-container datagrid-striped" data-ride="datagrid"></div>
  <div  class="pager" id="listPager" align="right"></div>
</div>
<!-- 添加页面  -->
<div class="modal fade" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">新增文件</h4>
      </div>
      <form id="addForm">
      <div class="modal-body">
        <div class="row with-padding">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					  <span class="input-group-addon">文件名称<font color="red">*</font></span>
					 <div  id="uploadHeadpic" class="uploader with-padding">
					  <div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
					  <button type="button" class="btn btn-primary uploader-btn-browse"><i class="icon icon-cloud-upload"></i> 选择文件</button>
					</div>
					 <input type="hidden" id="addHeadpic" name="addHeadpic">	
					  <input type="hidden" id="addHeadpic2" name="addHeadpic2">	<!-- 文件路径 -->
					   <input type="hidden" id="addHeadpic3" name="addHeadpic3"><!-- 文件后缀名 -->
				</div>
	 		</div>
	 	</div>
        <div class="row with-padding">
	 		<div class="col-md-12">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">描述<font color="red">*</font></span>
			 	 <textarea rows="5" cols="60" class="form-control"
					  id="addRemark" name="addRemark"></textarea>
				</div>
	 		</div>
	 		
	 	</div>
      </div>
      <div class="modal-footer">
      	
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary"  id="addSave">保存</button>
      </div>
       </form>
    </div>
  </div>
</div>

<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">删除地区</h4>
      </div>
      <div class="modal-body">
        <p>您确定要删除此地区信息吗?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="button" class="btn btn-primary" id="deleteFileBtn">确定</button>
      </div>
    </div>
  </div>
</div>
<!-- 详情页面  -->
<div class="modal fade" id="detailModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">文件详情</h4>
      </div>
      <form id="detailForm">
      <div class="modal-body">
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">文件名称</span>
					  <span class="form-control"  id="detailFileName"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">文件路径</span>
			 	  <span class="form-control"  id="detailFilePath"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">文件扩展名</span>
					  <span class="form-control"  id="detailFileType"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">文件备注</span>
				  <span class="form-control"  id="detailRemark"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">操作时间</span>
					  <span class="form-control"  id="detailOpr_time"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">操作人</span>
				  <span class="form-control"  id="detailOpr_id"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">用户号</span>
				  	  <span class="form-control"  id="detailPerUser_Code"></span>
				</div>
	 		</div>
	 	
	 	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
      </div>
    </form>
    </div>
  </div>
</div>
	
   	<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}"/>
   	<script type="text/javascript" src="${pageContext.request.contextPath}/js/admin.js?version=17"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/authority.js?version=1"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/file.js?version=2"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
</body>
</html>