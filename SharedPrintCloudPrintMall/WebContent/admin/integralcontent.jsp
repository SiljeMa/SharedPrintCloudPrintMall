<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- 兼容浏览器 -->
<meta http-equiv="X-UA-Compatiable" content="IE=edge">
<!-- 屏幕自适应 -->
<meta name="viewport" content="width=device-width,initial-scale=1" >
<title>云打印商城首页</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin.css?version=4">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css?version=3">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.datagrid.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.uploader.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.4.1.js"></script>
 	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.datagrid.js?version=12"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.chart.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js"></script>
<body >
	

<div class="panel ">
	 <div class="panel-heading">
	   	查询区域
	 </div>
	 <div class="panel-body row">
	 	<div class="row">
	 		<div class="col-md-3">
	 			<div class="input-group ">
					  <span class="input-group-addon">用户名</span>
					  <input type="text" class="form-control" id="queryClientName" name="queryClientName">
				</div>
	 		</div>
	 		<div class="col-md-3">
	 			<div class="input-group ">
					  <span class="input-group-addon">名称</span>
					  <input type="text" class="form-control" id="queryClientTrumName" name="queryClientTrumName">
				</div>
	 		</div>
	 			<div class="col-md-3">
	 			<div class="input-group ">
					  <span class="input-group-addon">积分类型</span>
					  <select  class="form-control" id="queryClientScore" name="queryClientScore">
			 	  	<option value="">请选择</option>
			 	  </select>
				</div>
	 		</div>
	 		
	 		<div class="col-md-3">
			    <button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
	 		</div>
	 	</div>
	 </div>
 </div>
 <div class="datagrid">
  <div class="search-box ">
  	
  </div>
  <div id="listDataGrid" class="datagrid-container datagrid-striped" data-ride="datagrid"></div>
  <div  class="pager" id="listPager" align="right"></div>
</div>
	
   	<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}"/>
   	<script type="text/javascript" src="${pageContext.request.contextPath}/js/admin.js?version=17"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/authority.js?version=1"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/client_integralcontent.js?version=1"></script>
    
</body>
</html>