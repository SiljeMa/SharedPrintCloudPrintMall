<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- 兼容浏览器 -->
<meta http-equiv="X-UA-Compatiable" content="IE=edge">
<!-- 屏幕自适应 -->
<meta name="viewport" content="width=device-width,initial-scale=1" >
<title>云打印商城首页</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin.css?version=4">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css?version=3">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.datagrid.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.uploader.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.4.1.js"></script>
 	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.datagrid.js?version=12"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.chart.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js"></script>
<body >
	
<div class="panel ">
	 <div class="panel-heading">
	   	查询区域
	 </div>
	 <div class="panel-body row">
	 	<div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">订单号</span>
					  <input type="text" class="form-control" id="queryOrderNo" name="queryOrderNo">
				</div>
	 		</div>
	 		
	 			<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">订单状态</span>
					  <select  class="form-control" id="queryOrderState" name="queryOrderState">
			 	  	<option value="">请选择</option>
			 	  </select>
				</div>
	 		</div>
	 		
	 	</div>
	 </div>
	 	 <div class="panel-body row">
	 	<div class="row">
	 		<div class="col-md-4">
	 			<div class="input-group ">
					  <span class="input-group-addon">订单创建时间前范围</span>
					  <input type="text" class="form-control" id="queryOrderCreateDate" name="queryOrderCreateDate">
				</div>
	 		</div>
	 		
	 			<div class="col-md-4">
	 			<div class="input-group ">
					  <span class="input-group-addon">订单创建时间后范围</span>
					 <input type="text" class="form-control" id="queryOrderEndDate" name="queryOrderEndDate">
				</div>
	 		</div>
	 		
	 		<div class="col-md-4">
			    <button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
	 		</div>
	 	</div>
	 </div>
 </div>
 <div class="datagrid">
  <div class="search-box ">
  	  	<button class="btn btn-info " type="button" id="detailBtn"><i class="icon icon-edit"></i>详情</button>
  </div>
  <div id="listDataGrid" class="datagrid-container datagrid-striped" data-ride="datagrid"></div>
  <div  class="pager" id="listPager" align="right"></div>
</div>
<div class="modal fade" id="detailModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">订单详情</h4>
      </div>
      <form id="detailForm">
      <div class="modal-body">
       <div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					  <span class="input-group-addon">订单主表信息</span>
					 
				</div>
	 		</div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">订单号</span>
					  <span class="form-control"  id="detailORDER_NO"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">订单状态</span>
			 	  <span class="form-control"  id="detailORDER_STATE"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">下单人编号</span>
					  <span class="form-control"  id="detailBUYER_ID"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">订单价格</span>
				  <span class="form-control"  id="detailORDER_PRICE"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">订单创建时间<span>
					  <span class="form-control"  id="detailORDER_CREATEDATE"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">订单修改时间</span>
				  <span class="form-control"  id="detailORDERCHANGEDATE"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">退款金额</span>
				  	  <span class="form-control"  id="detailREFUNDA_MOUNT"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">退款时间</span>
				  	  <span class="form-control"  id="detailREFUND_DATE"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	 <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">订单备注</span>
				  	  <span class="form-control"  id="detailORDERRE_MARK"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">审核者</span>
				  	  <span class="form-control"  id="detailASSESSOR"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">审核意见<span>
					  <span class="form-control"  id="detailOPINION"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">审核时间</span>
				  <span class="form-control"  id="detailASSESS_DATE"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">所属商户<span>
					  <span class="form-control"  id="detailAFFILIATED_MERCHANT"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">所属代理商</span>
				  <span class="form-control"  id="detailAFFILIATED_agent"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					  <span class="input-group-addon">所属机构<span>
					  <span class="form-control"  id="detailAFFILIATED_organization"></span>
				</div>
	 		</div>
	 	
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	<div class="col-md-12">
	 			<div class="input-group ">
					  <span class="input-group-addon">订单收货表信息</span>
					 
				</div>
	 		</div>
	 		<div style="margin-top:10px;"></div>
        <div class="row">
	 	
	 		<div class="col-md-12">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">收货人</span>
				  <span class="form-control"  id="detailCONSIGEE"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">电话<span>
					  <span class="form-control"  id="detailTELEPHONE"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">收货地址</span>
				  <span class="form-control"  id="detailADDRESS"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					  <span class="input-group-addon">操作时间<span>
					  <span class="form-control"  id="detailOPRDATE1"></span>
				</div>
	 		</div>
	 
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	<div class="col-md-12">
	 			<div class="input-group ">
					  <span class="input-group-addon">订单商品表信息</span>
					 
				</div>
	 		</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		
	 		<div class="col-md-12">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">商品名称</span>
				  <span class="form-control"  id="detailCOMMODITY_NAME"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">商品数量<span>
					  <span class="form-control"  id="detailCOMMODITY_COUNT"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">商品单价</span>
				  <span class="form-control"  id="detailCOMMODITY_CPRICE"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">商品总价<span>
					  <span class="form-control"  id="detailCOMMODITY_TOTALCPRICE"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">商品描述</span>
				  <span class="form-control"  id="detailCOMMODITY_CONTENT"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">所属商品类别<span>
					  <span class="form-control"  id="detailPRODUCT_CATEGORY_NAME"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">所属商品品牌</span>
				  <span class="form-control"  id="detailBRAND_NAME"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">图片一<span>
					  <span class="form-control"  id="detailPICTURE1"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">图片2</span>
				  <span class="form-control"  id="detailPICTURE2"></span>
				</div>
	 		</div> 
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">图片3<span>
					  <span class="form-control"  id="detailPICTURE3"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">图片4</span>
				  <span class="form-control"  id="detailPICTURE4"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">图片5<span>
					  <span class="form-control"  id="detailPICTURE5"></span>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">图片6</span>
				  <span class="form-control"  id="detailPICTURE6"></span>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					  <span class="input-group-addon">操作时间<span>
					  <span class="form-control"  id="detailOPRDATE2"></span>
				</div>
	 		</div>
	 	
	 	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
      </div>
    </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js?version=22"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/order/order.js?ver=322"></script>
	
	
   	<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}"/>
   	<script type="text/javascript" src="${pageContext.request.contextPath}/js/admin.js?version=17"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/authority.js?version=1"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/order.js?version=1"></script>
    
</body>
</html>