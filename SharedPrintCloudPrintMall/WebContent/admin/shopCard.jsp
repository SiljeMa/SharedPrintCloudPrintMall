<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>爱购物</title>

<link href="css/main.css" rel="stylesheet" type="text/css" />
<link href="css/zui.css" rel="stylesheet" type="text/css" />



<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/zui.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/admin.css?version=4">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/main.css?version=3">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/zui.datagrid.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/zui.uploader.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.4.1.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/zui.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/zui.datagrid.js?version=12"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/zui.chart.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/config.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/shopCar.js?ver=4"></script>
</head>
<body>
	<!-- 头部 -->
	<div class="guide">
		<nav class="navbar navbar-default" role="navigation">
		<div class="collapse navbar-collapse navbar-collapse-example">
			<ul class="nav navbar-nav nav-justified">
			   <li><a href="${pageContext.request.contextPath}/admin/admin.jsp"><font size="3">首页</font></a></li>
				<li><a href="${pageContext.request.contextPath}/admin/order.jsp"><font size="3">订单</font></a></li>
		        <li><a href="${pageContext.request.contextPath}/admin/shopCard.jsp"><font size="3">购物车</font></a></li>
		  
			</ul>
		</div>
		</nav>
	</div>

	<div id="header">
		<div id="nav">
			<div class="panel panel-primary">
				<div id="myNiceCarousel" class="carousel slide" data-ride="carousel">
					<!-- 圆点指示器 -->
					<ol class="carousel-indicators">
						<li data-target="#myNiceCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myNiceCarousel" data-slide-to="1"></li>
						<li data-target="#myNiceCarousel" data-slide-to="2"></li>
					</ol>

					<!-- 广告区 -->
					<!-- 轮播项目 -->
					<div class="carousel-inner">
						<div class="item active">
							<img alt="First slide"
								src="${pageContext.request.contextPath}/images/ads1.jpg">
							<div class="carousel-caption"></div>
						</div>
						<div class="item">
							<img alt="Second slide"
								src="${pageContext.request.contextPath}/images/ads2.jpg">
							<div class="carousel-caption"></div>
						</div>
						<div class="item">
							<img alt="Third slide"
								src="${pageContext.request.contextPath}/images/ads3.jpg">
							<div class="carousel-caption"></div>
						</div>
					</div>

					<!-- 项目切换按钮 -->
					<a class="left carousel-control" href="#myNiceCarousel"
						data-slide="prev"> <span class="icon icon-chevron-left"></span>
					</a> <a class="right carousel-control" href="#myNiceCarousel"
						data-slide="next"> <span class="icon icon-chevron-right"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- 没有数据 -->
	<div class="pager" id="nullData" align="right"></div>



	<div id="container">
	

		<div class="mainBoby">
			<div class="panel panel-primary" id="shopCardPanel">
				<div class="panel">
					<div class="panel-heading">购物车</div>
					<label id="nullDataText">购物车空空如也，快去添加点商品吧！</label>
					<!-- 购物车商品表的Id -->
					<input type="hidden" id="cardId" name="cardId">
					<div class="panel-body" id="cardShow">
						<!-- 购物车展示区域 -->
					</div>
				</div>
				<!-- 分页 -->
				<div class="pager" id="listPager" align="right"></div>
			</div>

			<div class="panel panel-primary" id="orderPanel">
				<div class="panel">
					<div class="panel-heading">确认订单</div>
					<form id="submitOrder">
						<div class="panel-body" id="orderShow">

							<!-- 确认订单展示区域 -->
							<div class="row">
								<div class="col-md-6">
									<div class="input-group ">
										<span class="input-group-addon">下单人<font color="red">*</font></span>
										<span class="form-control" id="orderPersonName"></span>

									</div>
								</div>
								<div class="col-md-6">
									<div class="input-group ">
										<span class="input-group-addon">订单号<font color="red">*</font></span>
										<span class="form-control" id="orderNo"></span>
										<input type="hidden" class="form-control" id="orderNoValue"
											name="orderNoValue">
									</div>
								</div>
							</div>
							<div style="margin-top:10px;"></div>
							<div class="row">
								<div class="col-md-6">
									<div class="input-group ">
										<span class="input-group-addon">收货人<font color="red">*</font></span>
										<input type="text" class="form-control" id="orderConsigee"
											name="orderConsigee">
									</div>
								</div>
								<div class="col-md-6">
									<div class="input-group ">
										<span class="input-group-addon">收货地址<font color="red">*</font></span>
										<input type="text" class="form-control" id="orderAddress"
											name="orderAddress">
									</div>
								</div>
							</div>
							<div style="margin-top:10px;"></div>
							<div class="row">
								<div class="col-md-6">
									<div class="input-group ">
										<span class="input-group-addon">联系电话<font color="red">*</font></span>
										<input type="text" class="form-control" id="orderPhone"
											name="orderPhone">
									</div>
								</div>
							</div>
							<div style="margin-top:10px;"></div>


						</div>


						<div style="margin:0 auto;width:900px;">
							<button class="btn" type="reset">重置</button>
							<button type="submit" class="btn btn-primary "
								data-dismiss="modal" id="submitBtn">提交</button>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>


</body>

</html>
