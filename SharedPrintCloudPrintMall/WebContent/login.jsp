<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head>
    <meta charset="utf-8">
    <!-- 自适应浏览器 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- 自适应屏幕 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>云打印商城登录页面</title>
    <!-- zui -->
    <link href="${pageContext.request.contextPath}/css/zui.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/main.css?version=3" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.uploader.css">
  </head>
  <body>
   <div class="login_tip">
   <div class="panel">
  		<div class="panel panel-primary">
  		<div class="panel-heading">
   		 	 <font size="5">云&nbsp;打&nbsp;印&nbsp;商&nbsp;城&nbsp;登&nbsp;录</font>
 		 </div>
  		<div class="panel-body">
    		
    		<div class="login_info">
    			<div class="input-control has-label-left">
				  <input id="loginName" name="loginName" type="text" class="form-control" placeholder="">
				  <label for="inputAccountExample2" class="input-control-label-left">用户名:</label>
				</div>
    		</div>
    		<div class="login_info">
    			<div class="input-control has-label-left has-label-right">
				  <input id=passWd name="passWd" type="password" class="form-control" placeholder="">
				  <label for="inputPasswordExample2" class="input-control-label-left">密码:</label>
				</div>
    		</div>
    		
    		<div class="login_info">
    			<div class="btn-group">
				  <button class="btn btn-primary" id="userLoginBtn" style="width: 150px;">用户登录</button>
				  <button class="btn btn-primary" id="registeBtn" style="margin-left: 1px; width: 150px;">用户注册</button>
				  <button class="btn btn-primary" id="managerLoginBtn" style="margin-left: 1px; width: 150px;">后台登录</button>
				</div>
    		</div>
    		
			<img src="images/shoplogo2.jpg" alt="Google" id="friend_logo" height="22px" />
			</form>
  		</div>
		</div>
   </div>
	</div>
<div id="header">
		
<div id="nav">
	<div class="panel panel-primary">
		 <div id="myNiceCarousel" class="carousel slide" data-ride="carousel">
	  <!-- 圆点指示器 -->
	  <ol class="carousel-indicators">
	    <li data-target="#myNiceCarousel" data-slide-to="0" class="active"></li>
	    <li data-target="#myNiceCarousel" data-slide-to="1"></li>
	    <li data-target="#myNiceCarousel" data-slide-to="2"></li>
	  </ol>
	  <!-- 轮播项目 -->
	  <div class="carousel-inner">
	    <div class="item active">
	      <img alt="First slide" src="images/ads1.jpg">
	      <div class="carousel-caption">
	    		
	      </div>
	    </div>
	    <div class="item">
	      <img alt="Second slide" src="images/ads2.jpg">
	      <div class="carousel-caption">
	        
	      </div>
	    </div>
	    <div class="item">
	      <img alt="Third slide" src="images/ads3.jpg">
	      <div class="carousel-caption">
	      </div>
	    </div>
	  </div>
	
	  <!-- 项目切换按钮 -->
		  <a class="left carousel-control" href="#myNiceCarousel" data-slide="prev">
		    <span class="icon icon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#myNiceCarousel" data-slide="next">
		    <span class="icon icon-chevron-right"></span>
		  </a>
		</div>
		</div>
	</div>
</div>
<div id="container">
	<div class="sidebar">

	</div>
	
	<div class="main">
		
	</div>
</div>

<div class="modal fade" id="choseRoleModal">
	<div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">选择后台系统</h4>
      </div>
      <div class="modal-body">
        	<div class="btn-group" data-toggle="buttons" id="roleDiv">
			  <label class="btn btn-info active" style="margin-left: 12px;">
			    <input type="radio" name="options" id="option1" value="0" checked>运营系统
			  </label>
			  <label class="btn btn-info" style="margin-left: 2px;">
			    <input type="radio" name="options" id="option2" value="1"> 代理商系统
			  </label>
			  <label class="btn btn-info" style="margin-left: 2px;">
			    <input type="radio" name="options" id="option3" value="2"> 商户系统
			  </label>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" id="cancelBtn" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="button" class="btn btn-primary"  id="chooseBtn">确定</button>
      </div>
    </div>
  </div>
</div>

<!-- <!-- 添加用户页面  -->
<div class="modal fade" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">用户注册</h4>
      </div>
      <form id="registerForm">
      <div class="modal-body">
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">姓名<font color="red">*</font></span>
			 	  <input type="text" class="form-control" id="addTrueName" name="addTrueName">
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">用户名<font color="red">*</font></span>
					  <input type="text" class="form-control" id="addLoginName" name="addLoginName">
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					  <span class="input-group-addon">所属机构<font color="red">*</font></span>
					  <select class="form-control" id="addUserOrgCode" name="addUserOrgCode">
					    <option value="">请选择</option>
					  </select>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	<div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
			 	  <span class="input-group-addon">所属地区<font color="red">*</font></span>
			 	  <span class="input-group-addon"></span>
					  <select class="form-control" id="addProvince" name="addProvince" 
					  style="width: 160px;">
					    <option value="">省</option>
				  	  </select>
				  <span class="input-group-addon"></span>
				  	  <select class="form-control" id="addCity" name="addCity" style="width: 160px;">
					    <option value="">市</option>
				  	  </select>
				  <span class="input-group-addon"></span>
				  	  <select class="form-control" id="addRegion" name="addRegion" style="width: 160px;">
					    <option value="">区</option>
				  	  </select>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	<div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">所属角色<font color="red">*</font></span>
					  <select class="form-control" id="addUserRoleCode" name="addUserRoleCode">
					    <option value="">请选择</option>
					  </select>
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					  <span class="input-group-addon">性别<font color="red">*</font></span>
					  <select class="form-control" id="addUserSex" name="addUserSex">
					    <option value="0">请选择</option>
					  </select>
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
        <div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	   <span class="input-group-addon">手机号码<font color="red">*</font></span>
			 	   <input type="text" class="form-control" id="addMobile" name="addMobile" >
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					 <span class="input-group-addon">邮箱<font color="red">*</font></span>
			 	     <input type="text" class="form-control" id="addEmail" name="addEmail" >
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	<div class="row">
	 		<div class="col-md-6">
	 			<div class="input-group ">
			 	   <span class="input-group-addon">QQ<font color="red">*</font></span>
			 	   <input type="text" class="form-control" id="addQq" name="addQq" >
				</div>
	 		</div>
	 		<div class="col-md-6">
	 			<div class="input-group ">
					 <span class="input-group-addon">微信<font color="red">*</font></span>
			 	     <input type="text" class="form-control" id="addWeixin" name="addWeixin" >
				</div>
	 		</div>
	 	</div>
	 	<div style="margin-top:10px;"></div>
	 	 <div class="row">
	 		<div class="col-md-12">
	 			<div class="input-group ">
					 <span class="input-group-addon">地址<font color="red">*</font></span>
			 	     <textarea rows="5" cols="60" class="form-control"
					  id="addAddress" name="addAddress"></textarea>
				</div>
	 		</div>
	 	</div>
      </div>
      <div class="modal-footer">
      	<input type="hidden" id="addAreaCode" name="addAreaCode" />
        <button type="button" id="cancelBtn" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary"  id="addSaveBtn">保存</button>
      </div>
       </form>
    </div>
  </div>
</div>

    <!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
    <script src="${pageContext.request.contextPath}/js/jquery-3.4.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
    <!-- ZUI Layer Javascript组件 -->
    <script src="${pageContext.request.contextPath}/js/zui.js"></script>
    
    <!-- url地址的配置文件 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js?ver=4"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/login.js?ver=22"></script>
   	<script type="text/javascript" src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
  </body>
</html>