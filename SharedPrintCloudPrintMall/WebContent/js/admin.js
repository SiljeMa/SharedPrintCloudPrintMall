$(document).ready(function(){
	
	//设置token以及ueserId
//	var token = $.zui.store.get("token");
//	var userId = $.zui.store.get('userId');
// 
	var token = $.zui.store.get('token');
	var userId = $.zui.store.get('userId');
	console.log('userId='+userId);
	console.log('token='+token);

	var userName = $.zui.store.get('userName');
	
	var windowHeight = $(document).height();
	$("#treeMenu").css("min-height",windowHeight-40);
	$("#userTask").append("<a href='your/nice/url' class='dropdown-toggle' data-toggle='dropdown'>" +
			"<font size='3' color='blue'>欢迎您，"+userName+"<b class= 'caret'></b></font></a>");
	

	var CatogoryID = '';
	//商品品牌value
	var BrandID = '';
	//动态获取商品类别列表
	var getCatogoryList = function(){
		var catogoryUrl = adminQueryUrl + categoryService + queryPageListMethod;
		$.ajax({
			url : catogoryUrl, 
			type : 'post',
			dataType : 'json',
			beforeSend : function(xhr) { //设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers : { 
				'token' : token,
				'userId' : userId
			},
			success : function(data) {
				var result = '';
				var catogoryList = data.data;
				if (catogoryList != null && catogoryList.length > 0) {
					$("#CatogoryList").append("<li id='0' class='active' value=''><a>商品类别</a></li>");
					$.each(catogoryList, function(i, item){
						$("#CatogoryList").append("<li value='"+item.Category_ID+"'><a>"+item.Category_Name+"<span class='label label-badge label-success'></span></a></li>");
					});
						$("#CatogoryList li").click(function(){
							CatogoryID = $(this).val();
							//alert("CatogoryID="+CatogoryID);
							queryGoodsListFun();
							//点击完之后清空
							//CatogoryID = '';
						});
				}else{
					new $.zui.Messager("系统繁忙",{
						type:'warning',
						placement: "center"
						
					});
				}
			},
			error : function(e) {
				new $.zui.Messager('系统繁忙,请稍候再试!', {
					type : 'warning',
					placement : 'center'
				}).show();
			}
		});
	}
	

	var getBrandList = function(){
		//动态获取商品品牌列表
	var brandUrl = adminQueryUrl + bandService + queryPageListMethod;
		$.ajax({
			url : brandUrl, 
			type : 'post',
			dataType : 'json',
			beforeSend : function(xhr) { //设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers : { 
				'token' : token,
				'userId' : userId
			},
			success : function(data) {
				var result = '';
				var brandList = data.data;
				if (brandList != null && brandList.length > 0) {
					$("#BrandList").append("<li class='active'><a>品牌</a></li>");
					$.each(brandList, function(i, item){
						$("#BrandList").append("<li value='"+item.Band_ID+"'><a>"+item.Band_Name+"<span class='label label-badge label-success'></span></a></li>");
					});
						$("#BrandList li").click(function(){
							BrandID = $(this).val();
							//alert("BrandID="+BrandID);
							queryGoodsListFun();
							//点击完之后清空
						});
				}else{
					new $.zui.Messager("系统繁忙",{
						type:'warning',
						placement: "center"
						
					});
				}
			},
			error : function(e) {
				new $.zui.Messager('系统繁忙,请稍候再试!', {
					type : 'warning',
					placement : 'center'
				}).show();
			}
		});
	}
	
	
	
	var pageSize = 9;
	var currentPage = 1;
	
	//用于商品分页查询
	var queryGoodsListFun = function(){
		var goodsUrl = adminQueryUrl + goodsService + queryPageListMethod;
		console.log('goodsUrl='+goodsUrl);
		var param =  "pageSize="+pageSize;//请求到列表页面
		//alert("aaa"+BrandID);
		if ($("#queryGoodsNameLike").val()!=''){
			param += "&queryGoodsNameLike="+$("#queryGoodsNameLike").val()
		}
		if (BrandID!=''){
			param += "&queryBandId="+BrandID;
		}
		if (CatogoryID!=''){
			param += "&queryCategoryId="+CatogoryID
		}
		console.log("param="+param)
		$.ajax({
			url : goodsUrl,
			type : 'post',
			dataType : 'JSON',
			beforeSend : function(xhr) { //设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers : {
				'token' : token,
				'userId' : userId
			},
			data : param,
			success : function(data) {
				console.log('传回来的菜单表格数据=' + data);
				$('#MainBox').empty();
				if (data != null && data.result == "success") {
					var goodsList = data.data;
						$.each(goodsList, function(i, item){
							$("#MainBox").append("<div class='col-md-4 goodsBox'>" +
								"<table style='width: 210px; margin-top: 5px;'>" +
									"<tr>" +
										"<td colspan='2' height='200px' width='210px;'><img id='"+item.Goods_ID+"' src='"+adminQueryUrl+"/"+item.Goods_Picture1+"'/></td>" +
									"</tr>" +
									"<tr>" +
										"<td colspan='2' height='30px;' width='210px'>" +
											"<a id='"+item.Goods_ID+"'><font size='2' color='black'>"+item.Goods_Name+"</font></a>" +
										"</td>" +
									"</tr>" +
								"</table>" +
							"</div>");
						});
							//图片点击事件
							$("#MainBox img").click(function(){
								var goodsId = $(this).attr("id");
								showGoodsInfo(goodsId);
							});
							//商品名称点击事件
							$("#MainBox a").click(function(){
								var goodsId = $(this).attr("id");
								showGoodsInfo(goodsId);
							});
						$("#BrandList").empty();
						$("#CatogoryList").empty();
						getBrandList();
						getCatogoryList();
					//分页
					if (parseInt(data.pager.recTotal) > 0) {
						$('#goodsPager').pager({
							elements : [ 'first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text', 'size_menu', 'goto' ],
							page : currentPage,
							pageSizeOptions : [ 1,5, 10, 20, 30, 50, 100 ],
							recTotal : data.pager.recTotal,
							recPerPage : pageSize
						});
						// 获取分页器实例对象
						var myPager = $('#goodsPager').data('zui.pager');
						myPager.set(currentPage, data.pager.recTotal, pageSize);
					} else {
						$("#MainBox").html("查无数据");
					}
				} else {
					new $.zui.Messager("系统繁忙", {
						type : 'warning',
						placement : "center"
					}).show();
				}
			},
			error : function(e) {
				new $.zui.Messager('系统繁忙,请稍候再试!', {
					type : 'warning',
					placement : 'center'
				}).show();
			}
		});
	};
	queryGoodsListFun();
	
	//显示商品详情
	var showGoodsInfo = function(goodsId){
		var goodsUrl = adminQueryUrl + goodsService + detailMethod;
		console.log('goodsUrl='+goodsUrl);
		var param = 'goodsId='+goodsId;//请求到列表页面
			console.log("goodsUrl="+goodsUrl+",param="+param);
			$.ajax({
				url:goodsUrl,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token", token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					if (data.success=='1'){
						$('#detailModal').modal('show','fit');
						console.log('data='+data);
						$("#imgList").html("<div class='item active'><img alt='First slide' src='"+adminQueryUrl+"/"+data.Goods.goodsPicture1+"'><div class='carousel-caption'></div></div>");
						$("#detailGoodsName").html(data.Goods.goodsName);
						$("#detailGoodsPrice").html(data.Goods.goodsPrice);
						$("#detailBrand").html(data.goodsBandName);
						$("#detailCatogory").html(data.goodsCategoryName);
						$("#detailDescription").html(data.Goods.goodsDescripe);
						$("#addShopCard").val(data.Goods.goodsId);
					
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
	}
	
	//监听分页，修改当前页、条数
	$('#goodsPager').on('onPageChange',function(e, state, oldState){
		currentPage = state.page;
		pageSize = state.recPerPage;
		queryGoodsListFun();
	});
	
	//商品搜索
	$('#searchBtn').click(function(){
		queryGoodsListFun();
	});
	
	//商品搜索
	$('#loginOut').click(function(){
		if(confirm("您确定要退出登陆吗?")){
			$.zui.store.clear();
			location.href='../login.jsp';
		}
	});
	
	//初始化注册中的下拉框
	$("#register").click(function(){
		//获取菜单列表
		$('#addModal').modal('show', 'fit');
		//重置表单
		document.getElementById("registerForm").reset();
		var initUrl = adminQueryUrl + adminUserService + initAddMethod;
		var param = "";	//请求到列表页面
		console.log("initUrl"+initUrl+param);
		$.ajax({
			url:initUrl,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success=='1'){
					//重置省份下拉框
					$("#addProvince").empty();
					var value = "";
					$("#addProvince").html("<option value="+value+">省</option>");
					//重置城市下拉框
					$("#addCity").empty();
					$("#addCity").html("<option value="+value+">市</option>");
					//重置市区下拉框
					$("#addRegion").empty();
					$("#addRegion").html("<option value="+value+">区</option>");
					//自动生成编码
					$("#addOrgCode").val(data.randomCode);
					
					$.each(data.provinceList, function(i, item){    
						$("#addProvince").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
					});
					//初始化机构下拉框
					$("#addUserOrgCode").empty();
					$("#addUserOrgCode").html("<option value="+value+">请选择</option>");
					$.each(data.orgList, function(i, item){     
						$("#addUserOrgCode").append("<option value='"+item.Org_Code+"'>"+item.Org_Name+"</option>");
					}); 
					//初始化性别下拉框
					$("#addUserSex").empty();
					$("#addUserSex").html("<option value="+value+">请选择</option>");
					$.each(data.sexList, function(i, item){     
						$("#addUserSex").append("<option value='"+item.KEY_VALUE+"'>"+item.KEY_NAME+"</option>");
					}); 
					//初始化角色下拉框
					$("#addUserRoleCode").empty();
					$("#addUserRoleCode").html("<option value="+value+">请选择</option>");
					$.each(data.roleList, function(i, item){     
						$("#addUserRoleCode").append("<option value='"+item.Role_ID+"'>"+item.RoleName+"</option>");
					}); 
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	
	//根所选省份获取对应的城市
		var provinceSel = document.getElementById("addProvince");
		provinceSel.onchange=function(){
				var provinceCode = provinceSel.options[provinceSel.selectedIndex].value;
				showCity(provinceCode);
		}
		//根所选城市获取对应的市区
		var CitySel = document.getElementById("addCity");
		CitySel.onchange=function(){
				var CityCode = CitySel.options[CitySel.selectedIndex].value;
				showRegion(CityCode);
		}

	//根据所选城市显示对应的区
	function showRegion(CityCode){
		var initRegionUrl = adminQueryUrl + institutionService + initAddMethod;
		var param = "&cityCode="+CityCode;	//请求到列表页面
		console.log("initUrl"+initRegionUrl+"param="+param);
		$.ajax({
			url:initRegionUrl,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success=='1'){
				$("#addRegion").empty();
				var value = "";
				$("#addRegion").html("<option value="+value+">区</option>");
					$.each(data.regionList, function(i, item){     
						$("#addRegion").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
					});
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	}
	
	//根据所选省份获取对应的城市
		function showCity(ProviceCode){
			var initCityUrl = adminQueryUrl + institutionService + initAddMethod;
			var param = "&provinceCode="+ProviceCode;	//请求到列表页面
			console.log("initUrl"+initCityUrl+"param="+param);
			$.ajax({
				url:initCityUrl,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token", token);
					xhr.setRequestHeader("userId", userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$("#addCity").empty();
						var value = "";
						$("#addCity").html("<option value="+value+">市</option>");
						
						$("#addRegion").empty();
						$("#addRegion").html("<option value="+value+">区</option>");
						$.each(data.cityList, function(i, item){     
							$("#addCity").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
						});
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	
	//手机号验证
	jQuery.validator.addMethod("isMobile",function(value,element){
		if (value.length!=11){
			return false;
		}
		return true;
	},"手机号应为11位");
	
	//保存新增用户的信息
	$("#addSaveBtn").click(function(){
		var validLoginNameUrl = adminQueryUrl + adminUserService + "/validLoginName.action";
		console.log("validLoginNameUrl"+validLoginNameUrl);
		//表单验证
		$("#registerForm").validate({
			rules:{
				addLoginName:{
					"required":true,
					"remote":{
						url:validLoginNameUrl,
						type:'get',
						dataType:'json',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token", token);
							xhr.setRequestHeader("userId", userId);
						},
						headers:{'token':token,'userId':userId},
						data:{
							'addAreaCode':function(){
								//alert('$("#addMenuName").val()='+$("#addMenuName").val())
								return $("#addAreaCode").val();
							}
						}
					}
				},
				addTrueName:{
					"required":true
				},
				addUserOrgCode:{
					"required":true
				},
				addProvince:{
					"required":true
				},
				addUserRoleCode:{
					"required":true
				},
				addUserSex:{
					"required":true
				},
				addMobile:{
					"required":true,
					"isMobile":true
				},
				addEmail:{
					"email":true,
				}
			},
			messages:{
				addLoginName:{
					"required":"请输入用户名",
					"remote":"该用户名已经存在"
				},
				addTrueName:{
					"required":"请输入姓名"
				},
				addUserOrgCode:{
					"required":"请选择机构"
				},
				addProvince:{
					"required":"请选择省份,市区视情况选择"
				},
				addUserRoleCode:{
					"required":"请选择角色"
				},
				addUserSex:{
					"required":"请选择性别"
				},
				addMobile:{
					"required":"请填写手机号"
				}
			},
			//表单数据提交
			submitHandler:function(form){
				//form.submit();
				var addUrl = adminQueryUrl + adminUserService + "/add.action";
				console.log("userAddUrl"+addUrl);
				$.ajax({
					url:addUrl,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token", token);
						xhr.setRequestHeader("userId", userId);
					},
					headers:{'token':token,'userId':userId},
					//serialize()表单数据序列化
					data:$(form).serialize(),
					success:function(data){
						console.log('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('注册成功，初始密码为123456。', {
						    type: 'success',
						    placement:'center'
						}).show();
		                	$('#addModal').modal('hide', 'fit');
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});
	});
	
	//初始化修改菜单
	$("#updateInfo").click(function(){
			//获取菜单列表
			$('#updateModal').modal('show', 'fit');
			//重置表单
			document.getElementById("updateForm").reset();
			var updateUrl = adminQueryUrl + adminUserService + initUpdateMethod;
			var param = 'UserID='+userId;//请求到列表页面
			console.log('updateUrl='+updateUrl+',param='+param);
			$.ajax({
				url:updateUrl,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token", token);
					xhr.setRequestHeader("userId", userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#updateModal').modal('show', 'fit');
						//机构下拉框初始化
						$("#updateUserOrgCode").empty();
						$("#updateUserOrgCode").append('<option value="0">请选择</option>');
						$.each(data.orgList, function(i, item){   
							if (data.userMap.Org_Code == item.Org_Code){
								$("#updateUserOrgCode").append("<option value='"+item.Org_Code+"' selected='selected'>"+item.Org_Name+"</option>");
							}
							else {
								$("#updateUserOrgCode").append("<option value='"+item.Org_Code+"'>"+item.Org_Name+"</option>");
							}
						});
						//性别下拉框初始化
						$("#updateUserSex").empty();
						$("#updateUserSex").append('<option value="">请选择</option>');
						$.each(data.sexList, function(i, item){   
							if (data.userMap.Sex == item.KEY_VALUE){
								$("#updateUserSex").append("<option value='"+item.KEY_VALUE+"' selected='selected'>"+item.KEY_NAME+"</option>");
							}
							else {
								$("#updateUserSex").append("<option value='"+item.KEY_VALUE+"'>"+item.KEY_NAME+"</option>");
							}
						});
						//角色下拉框初始化
						$("#updateUserRoleCode").empty();
						$("#updateUserRoleCode").append('<option value="">请选择</option>');
						$.each(data.roleList, function(i, item){   
							if (data.UserRoleID == item.Role_ID){
								$("#updateUserRoleCode").append("<option value='"+item.Role_ID+"' selected='selected'>"+item.RoleName+"</option>");
							}
							else {
								$("#updateUserRoleCode").append("<option value='"+item.Role_ID+"'>"+item.RoleName+"</option>");
							}
						});
						
						//地区下拉列表初始化
						//如果所属地区为 省 的话则城市下拉框初始化为对应省份的城市
						if(typeof(data.isProvinceCode)!="undefined"){
							var value = "";
							$("#updateProvince").empty();
							$("#updateProvince").html("<option value="+value+">省</option>");
							//省份列表初始化
							$.each(data.provinceList, function(i, item){ 
								if (data.isProvinceCode == item.Area_Code){
									$("#updateProvince").append("<option value='"+item.Area_Code+"' selected='selected'>"+item.Area_Name+"</option>");
								}
								else {
									$("#updateProvince").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
								}
							});
							//城市列表初始化
							$("#updateCity").empty();
							$("#updateCity").html("<option value="+value+">市</option>");
							$.each(data.cityList, function(i, item){ 
									$("#updateCity").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
							});
						}
						//所属地区为 市 的话则省份的默认值显示为该市所属的省份，区的下拉框初始化为对应城市的区域
						else if(typeof(data.isCityCode )!="undefined"){
							var value = "";
							$("#updateProvince").empty();
							$("#updateProvince").html("<option value="+value+">省</option>");
							//省份列表初始化
							$.each(data.provinceList, function(i, item){
								if (data.fatherAreaCode == item.Area_Code){
									$("#updateProvince").append("<option value='"+item.Area_Code+"' selected='selected'>"+item.Area_Name+"</option>");
								}
								else {
									$("#updateProvince").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
								}
							}); 
							//城市列表初始化
							$("#updateCity").empty();
							$("#updateCity").html("<option value="+value+">市</option>");
							$.each(data.cityList, function(i, item){ 
								if (data.isCityCode == item.Area_Code){
									$("#updateCity").append("<option value='"+item.Area_Code+"' selected='selected'>"+item.Area_Name+"</option>");
								}
								else {
									$("#updateCity").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
								}
							});
							//区列表初始化
							$("#updateRegion").empty();
							$("#updateRegion").html("<option value="+value+">区</option>");
							$.each(data.regionList, function(i, item){    
									$("#updateRegion").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");  
							});
					}
					//所属地区为 区 的话则显示所对应的省、城市
					else if(typeof(data.isRegionCode)!="undefined"){
						var value = "";
							$("#updateProvince").empty();
							$("#updateProvince").html("<option value="+value+">省</option>");
							//省份列表初始化
							$.each(data.provinceList, function(i, item){
								if (data.fatherProvinceCode == item.Area_Code){
									$("#updateProvince").append("<option value='"+item.Area_Code+"' selected='selected'>"+item.Area_Name+"</option>");
								}
								else {
									$("#updateProvince").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
								}
							}); 
							//城市列表初始化
							$("#updateCity").empty();
							$("#updateCity").html("<option value="+value+">市</option>");
							$.each(data.cityList, function(i, item){ 
								if (data.fatherCityCode == item.Area_Code){
									$("#updateCity").append("<option value='"+item.Area_Code+"' selected='selected'>"+item.Area_Name+"</option>");
								}
								else {
									$("#updateCity").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
								}
							});
							//区列表初始化
							$("#updateRegion").empty();
							$("#updateRegion").html("<option value="+value+">区</option>");
							$.each(data.regionList, function(i, item){    
								if (data.isRegionCode == item.Area_Code){
									$("#updateRegion").append("<option value='"+item.Area_Code+"' selected='selected'>"+item.Area_Name+"</option>");
								}
								else {
									$("#updateRegion").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
								}
							});
					}
						$("#updateTrueName").val(data.userMap.trueName);
						$("#updateLoginName").val(data.userMap.LoginName);
						$("#updateMobile").val(data.userMap.Mobile);
						$("#updateEmail").val(data.userMap.Email);
						$("#updateQq").val(data.userMap.Qq);
						$("#updateWeixin").val(data.userMap.Weixin);
						$("#updateAddress").val(data.userMap.Address);
						$("#updateWeixin").val(data.userMap.Weixin);
						$("#updateUserId").val(data.userMap.User_ID);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
	});
	
		//根所选省份获取对应的城市
		var updateProvinceSel = document.getElementById("updateProvince");
		updateProvinceSel.onchange=function(){
				var provinceCode = updateProvinceSel.options[updateProvinceSel.selectedIndex].value;
				showUpdateCity(provinceCode);
		}
		//根所选城市获取对应的市区
		var UpdateCitySel = document.getElementById("updateCity");
		UpdateCitySel.onchange=function(){
				var CityCode = UpdateCitySel.options[UpdateCitySel.selectedIndex].value;
				showUpdateRegion(CityCode);
		}

	//根据所选城市显示对应的区
	function showUpdateRegion(CityCode){
		var initRegionUrl = adminQueryUrl + institutionService + initUpdateMethod;
		var param = "&cityCode="+CityCode;	//请求到列表页面
		console.log("initUrl"+initRegionUrl+"param="+param);
		$.ajax({
			url:initRegionUrl,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success=='1'){
				$("#updateRegion").empty();
				var value = "";
				$("#updateRegion").html("<option value="+value+">区</option>");
					$.each(data.UpdateRegionList, function(i, item){     
						$("#updateRegion").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
					});
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	}
	
	//根据所选省份获取对应的城市
		function showUpdateCity(ProviceCode){
			var initCityUrl = adminQueryUrl + institutionService + initUpdateMethod;
			var param = "&provinceCode="+ProviceCode;	//请求到列表页面
			console.log("initUrl"+initCityUrl+"param="+param);
			$.ajax({
				url:initCityUrl,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token", token);
					xhr.setRequestHeader("userId", userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$("#updateCity").empty();
						var value = "";
						$("#updateCity").html("<option value="+value+">市</option>");
						
						$("#updateRegion").empty();
						$("#updateRegion").html("<option value="+value+">区</option>");
						$.each(data.UpdateCityList, function(i, item){     
							$("#updateCity").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
						});
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	
	//保存修改的信息
	$("#updateSave").click(function(){
		var validUserNameUrl = adminQueryUrl + adminUserService + "/validLoginName.action";
		console.log("validUserNameUrl="+validUserNameUrl);
		$("#updateForm").validate({
			rules:{
				updateLoginName:{
					"required":true,
					"remote":{
						url:validUserNameUrl,
						type:'get',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token", token);
							xhr.setRequestHeader("userId", userId);
						},
						headers:{'token':token,'userId':userId},
						dataType:'json',
						data:{
							'updateLoginName':function(){
								return $("#updateLoginName").val();
							},
							'excludeUserId':function(){
								return $("#updateUserId").val();
							}
						}
					}
				},
				updateTrueName:{
					"required":true
				},
				updateUserOrgCode:{
					"required":true
				},
				updateProvince:{
					"required":true
				},
				updateUserRoleCode:{
					"required":true
				},
				updateUserSex:{
					"required":true
				},
				updateMobile:{
					"required":true,
					"isMobile":true
				},
				updateEmail:{
					"email":true,
				}
			},
			messages:{
				updateLoginName:{
					"required":"请输入用户名",
					"remote":"该用户名已经存在"
				},
				updateTrueName:{
					"required":"请输入姓名"
				},
				updateUserOrgCode:{
					"required":"请选择机构"
				},
				updateProvince:{
					"required":"请选择省份,市区视情况选择"
				},
				updateUserRoleCode:{
					"required":"请选择角色"
				},
				updateUserSex:{
					"required":"请选择性别"
				},
				updatesMobile:{
					"required":"请填写手机号"
				}
			},
			submitHandler:function(form){
				$.ajax({
					url:adminQueryUrl + adminUserService + updateMethod,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token", token);
						xhr.setRequestHeader("userId", userId);
					},
					headers:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						console.log('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('修改成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#updateModal').modal('hide', 'fit');
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});
	});
	
	//修改密码验证
	$('#updatePwd').click(function(){
		
		var updateUrl = adminQueryUrl + adminUserService + initUpdateMethod;
			var param = 'UserID='+userId;//请求到列表页面
			console.log('updateUrl='+updateUrl+',param='+param);
			$.ajax({
				url:updateUrl,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token", token);
					xhr.setRequestHeader("userId", userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#updatePwdModal').modal('show','fit');
						//重置表单
						document.getElementById("updatePwdForm").reset();
						//获取初始密码
						$("#initPwd").val(data.userMap.Passwd);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
	});
	
	//保存修改后的密码
	$('#updatePwdSave').click(function(){
		var validPwdUrl = adminQueryUrl + adminUserService + "/validPwd.action";
		console.log("validPwdUrl="+validPwdUrl);
		$("#updatePwdForm").validate({
			rules:{
				newPwd:{
					"required":true,
					"remote":{
						url:validPwdUrl,
						type:'get',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token", token);
							xhr.setRequestHeader("userId", userId);
						},
						headers:{'token':token,'userId':userId},
						dataType:'json',
						data:{
							'newPwd':function(){
								return $("#newPwd").val();
							},
							'reInputPwd':function(){
								return $("#reInputPwd").val();
							}
						}
					}
				},
				reInputPwd:{
					"required":true
				}
			},
			messages:{
				newPwd:{
					"required":"请输入新密码",
					"remote":"输入的两次密码不相同，请确认后再次输入"
				},
				reInputPwd:{
					"required":"请再次输入密码"
				}
			},
			submitHandler:function(form){
				$.ajax({
					url:adminQueryUrl + adminUserService + "/updatePwd.action",
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token", token);
						xhr.setRequestHeader("userId", userId);
					},
					headers:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						console.log('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('修改成功!请重新登录', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#updatePwdModal').modal('hide', 'fit');
		                	location.href=adminCloudPrint+'/login.jsp';
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});
	});
	
	
	$("#addShopCard").click(function(){
		var goodsId = $("#addShopCard").val();
		var param = "goodsId="+goodsId;
		//点击后加入购物车表
		$.ajax({
			url:adminQueryUrl + shoppingCardService + addMethod,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
			
				if (data.success=='1'){
					new $.zui.Messager('添加购物车成功!', {
					    type: 'success',
					    placement:'center'
					}).show();
				}else{
					new $.zui.Messager('添加购物车失败!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	
		
	});
	
	
	
});

//退出登录
function logout(){
	if(confirm("您确定要退出登录吗?")){
		location.href=adminCloudPrint+'/login.jsp';
	}
}
