
//后台请求地址
var adminQueryUrl = 'http://localhost:8080/SharedPrintService';//工程是  SharedPrintService2  

//运营系统登录请求
var adminOperationManager = 'http://localhost:8080/SharedPrintOperationManager';
//代理商系统登录请求
var adminAgentManager = 'http://localhost:8080/SharedPrintAgentManager';
//商户系统登录请求
var adminMerchantManager = 'http://localhost:8080/SharedPrintMerchantManager';
//云打印商城登录请求
var adminCloudPrint = 'http://localhost:8080/SharedPrintCloudPrintMall';


//通用的增删改查
var queryPageListMethod = '/queryPageList.action';
var initAddMethod = '/initAdd.action';
var addMethod = '/add.action';
var initUpdateMethod = '/initUpdate.action';
var updateMethod = '/update.action';
var deleteMethod = '/delete.action';
var detailMethod = '/detail.action';
var checkMethod = '/check.action';
var validCode = '/validCode.action';
var validName = '/validName.action';
var validStatus ='/validStatus.action';

//获取用户方法
var adminUserService = '/api/userInfo';
var loginMethod = '/login.action';


//获取商品类别服务
var categoryService = '/api/categoryInfo';
//获取商品品牌服务
var bandService = '/api/bandInfo';
//获取商品服务
var goodsService = '/api/goodsInfo';
var checkUpdate = '/checkUpdate.action';//审核成功后，提交审核信息


//获取菜单服务
var menuService = '/api/menuInfo';
var queryLeftMenuMethod = '/queryLeftMenuList.action';

//获取地区服务
var areaService = '/api/areaInfo';

//获取机构服务
var institutionService = '/api/institutionInfo';

//获取代理商服务
var AgeService = '/api/ageInfo';
//获取商户服务
var merchantService = '/api/merchantInfo';
var unfreezeMethod = '/unfreeze.action';
var freezeMethod = '/freeze.action';
var checkMethod = '/check.action';
//获取商户用户服务
var merUserService = '/api/merUserInfo';
var resetMethod = '/reset.action';
//获取商户设备服务
var merDevService = '/api/merDevInfo';
//获取角色服务
var roleService='/api/roleInfo'
  
//获取设备厂商服务
var devicefactoryService = '/api/deviceFrimInfo';
//获取设备信息服务
var deviceinfoService = '/api/deviceInfo';
//获取设备故障服务

//获取设备型号服务
var devicemodelService = '/api/deviceModelInfo';


//获取购物车服务
var shoppingCardService = '/api/shoppingCardInfo';
//获取购物车商品服务
var shoppingCardGoodsService = '/api/shoppingCardGoodsInfo';

//获取数据字典服务
var dicsService = '/api/dics';
var dicsSex = '/sex.action';
var dicsCheckStatus = '/checkStatus.action';
var dicsUserStatus = '/userStatus.action';
var dicsHobby = '/hooby.action';

//获取代理商用户服务
var AgeUserService = '/api/ageuserInfo';


var unfreeze='/unfreeze.action';//解冻动作
var freeze='/freeze.action';//冻结动作
var clientService ='/api/client';//获取客户服务
var clientScore='/api/clientscore';//获取客户积分服务

var order='/api/order';//获取订单服务



var resetPwd='/resetPwd.action';//客户重置密码

var clientScorecontent='/api/clientscorecontent';//获取客户积分详细服务

//获取文件服务
var fileService = '/api/file';
var uploadUserHeadpic = '/uploadUserHeadpic.action';
var file='/file';
