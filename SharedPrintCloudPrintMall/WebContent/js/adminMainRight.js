var data = {
		    // labels 数据包含依次在X轴上显示的文本标签
		    labels: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
		    datasets: [{
		        // 数据集名称，会在图例中显示
		        label: "红队",

		        // 颜色主题，可以是'#fff'、'rgb(255,0,0)'、'rgba(255,0,0,0.85)'、'red' 或 ZUI配色表中的颜色名称
		        // 或者指定为 'random' 来使用一个随机的颜色主题
		        color: "red",
		        // 数据集
		        data: [65, 59, 80, 81, 56, 55, 40, 44, 55, 70, 30, 40]
		    }, {
		        label: "绿队",
		        color: "green",
		        data: [28, 48, 40, 19, 86, 27, 90, 60, 30, 44, 50, 66]
		    }]
		};

		var options = {}; // 图表配置项，可以留空来使用默认的配置

		var myLineChart = $("#myLineChart").lineChart(data, options);
	
var data2 = {
	    labels: ["一月", "二月", "三月", "四月", "五月", "六月", "七月"],
	    datasets: [
	        {
	            label: "蓝队",
	            color: 'blue',
	            data: [65, 59, 80, 81, 56, 55, 40]
	        }, {
	            label: "绿队",
	            color: 'green',
	            data: [28, 48, 40, 19, 86, 27, 90]
	        }
	    ]
	};

	var options2 = {responsive: true}; // 图表配置项，可以留空来使用默认的配置
	var myBarChart = $('#myBarChart').barChart(data2, options2);