$(document).ready(function() {
//确认订单区域一开始隐藏
	$("#orderPanel").hide();
	var currentPage = 1;
	var pageSize = 3;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");

	//用于分页查询
	var queryListFun = function() {
		$('#listPager').hide();
		$("#nullDataText").hide();
		var url = adminQueryUrl + shoppingCardService + queryPageListMethod;
		var param = "pageSize=" + pageSize; //请求到列表页面
		if (currentPage > 0) {
			param += "&currentPage=" + (currentPage - 1);
		}
		$.ajax({
			url : url,
			type : 'post',
			dataType : 'JSON',
			beforeSend : function(xhr) { //设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers : {
				'token' : token,
				'userId' : userId
			},
			data : param,
			success : function(data) {
				$('#panel-body').empty();
				if (data != null && data.result == "success") {
					//置空
					$('#listPager').data('zui.datagrid', null);
					if (parseInt(data.pager.recTotal) > 0) {
						
						$('#listPager').show();
						var str = '';
						$.each(data.data, function(index, value) {
							str += '<div class="panel panel-primary">';
							str += '<div class="panel-heading">' + value.SHOPPINGCARD_GOODS_NAME + '</div>';
							str += '<div class="panel-body">';
							str += '<div style="float:left;width:150px;"><img  src="' + adminQueryUrl + '/' + value.SHOPPINGCARD_GOODS_PICTURE1 + '"></div>';
							str += '<div style="float:right">';
							str += '<div style="text-align:left;">' + value.SHOPPINGCARD_GOODS_DESCRIPE + '</div>';
							str += '<div style="text-align:right;">数量:';
			str += '<i class="icon icon-minus" onclick="minus(' + value.SHOPPINGCARD_GOODS_ID + ',' + value.SHOPPINGCARD_ID + ',' + value.SHOPPINGCARD_GOODS_PRICE + ')"></i>';
 
						
							
						//	str += '<label id="totalNumber' + value.SHOPPINGCARD_GOODS_ID + '">' + value.SHOPPINGCARD_GOODS_TOTALNUMBER + '</lable>';
							str += '<span id="totalNumber' + value.SHOPPINGCARD_GOODS_ID + '">' + value.SHOPPINGCARD_GOODS_TOTALNUMBER + '</span>'
							str += '<i class="icon icon-plus" onclick="add(' + value.SHOPPINGCARD_GOODS_ID + ',' + value.SHOPPINGCARD_ID + ',' + value.SHOPPINGCARD_GOODS_PRICE + ')"></i>';
							
							str += '</div>';
							str += '<div style="text-align:right;">单价:' + value.SHOPPINGCARD_GOODS_PRICE + '</div>';
							str += '</div>';
							str += '</div>';
							str += '</div>';
						});
			
						str += '<div style="float:right">合计:<label id="totalPrice">' + data.data[0].SHOPPINGCARD_MONEY + '</label>&nbsp&nbsp<button class="btn btn-primary " type="button" onclick="submitShopCar()">结算</button></div>';
						str+='<div><button class="btn btn-primary " type="button" onclick="deleteAll()">清空购物车</button>&nbsp&nbsp';
						str+='</div>';
						//////隐藏域赋值
						$("#cardId").val(data.data[0].SHOPPINGCARD_ID);
						$('#cardShow').html(str);
						$('#listPager').pager({
							elements : [ 'first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text', 'size_menu', 'goto' ],
							page : currentPage,
							pageSizeOptions : [ 1, 5, 10, 20, 30, 50, 100 ],
							recTotal : data.pager.recTotal,
							recPerPage : pageSize
						});
						// 获取分页器实例对象
						var myPager = $('#listPager').data('zui.pager');
						myPager.set(currentPage, data.pager.recTotal, pageSize);
					} else {
						$("#nullDataText").show();
					}
				} else {
					new $.zui.Messager("系统繁忙", {
						type : 'warning',
						placement : "center"
					}).show();
				}
			},
			error : function(e) {
				new $.zui.Messager('系统繁忙,请稍候再试!', {
					type : 'warning',
					placement : 'center'
				}).show();
			}
		});
	};
	queryListFun();

	//监听分页，修改当前页、条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		queryListFun();
	});

	
	$('#submitBtn').click(function() {
		$("#submitOrder").validate({
			rules : {
				orderConsigee : {
					"required" : true,
				},
				orderAddress:{
					"required" : true,
				},
				orderPhone:{
					"required" : true,
					"digits" : true
				}
			},
			messages : {
			
			},
			submitHandler : function(form) {
				//表单序列化
				alert($.zui.store.get("userId")+'&totalPrice='+$("#totalPrice").html());
				var formParam = decodeURIComponent($(form).serialize(), true);
				var url = adminQueryUrl + order + '/addOrder.action';
				var param = formParam+'&cardId='+$("#cardId").val()+'&buyId='+$.zui.store.get("userId")+'&totalPrice='+$("#totalPrice").html();
				$.ajax({
					url : url,
					type : 'post',
					dataType : 'JSON',
					beforeSend : function(xhr) { //设置请求头信息
						xhr.setRequestHeader("token", token);
						xhr.setRequestHeader("userId", userId);
					},
					headers : {
						'token' : token,
						'userId' : userId
					},
					data : param,
					success : function(data) {
						if (data.success == "1") {
						new $.zui.Messager('结账成功!', {
							type : 'success',
							placement : 'center'
						}).show();
					//	location.href("");
						} else {
							new $.zui.Messager('系统繁忙,请稍候再试!', {
								type : 'success',
								placement : 'center'
							}).show();
						}
					},
					error : function(e) {
						new $.zui.Messager('系统繁忙,请稍候再试!', {
							type : 'warning',
							placement : 'center'
						}).show();
					}
				});
			
			}
		});
	});
});

function minus(SHOPPINGCARD_GOODS_ID, SHOPPINGCARD_GOODS_CARDID, SHOPPINGCARD_GOODS_PRICE) {
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var totalNumber = $("#totalNumber" + SHOPPINGCARD_GOODS_ID).text();
	var totalNumber = parseInt(totalNumber) - 1;
	if (totalNumber < 1) {
		return;
	} else {
		var url = adminQueryUrl + shoppingCardService + '/updateNumber.action';
		var param = 'todo=minus&SHOPPINGCARD_GOODS_CARDID='
			+ SHOPPINGCARD_GOODS_CARDID + '&SHOPPINGCARD_GOODS_ID='
			+ SHOPPINGCARD_GOODS_ID + '&SHOPPINGCARD_GOODS_PRICE=' + SHOPPINGCARD_GOODS_PRICE;
		$.ajax({
			url : url,
			type : 'post',
			dataType : 'JSON',
			beforeSend : function(xhr) { //设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers : {
				'token' : token,
				'userId' : userId
			},
			data : param,
			success : function(data) {
				if (data.success == "1") {
					$("#totalNumber" + SHOPPINGCARD_GOODS_ID).text(totalNumber);
					var totalPrice = $("#totalPrice").html();
					var price = parseInt(totalPrice) - SHOPPINGCARD_GOODS_PRICE;
					$("#totalPrice").html(price);

				} else {
					new $.zui.Messager('系统繁忙,请稍候再试!', {
						type : 'warning',
						placement : 'center'
					}).show();
				}
			},
			error : function(e) {
				new $.zui.Messager('系统繁忙,请稍候再试!', {
					type : 'warning',
					placement : 'center'
				}).show();
			}
		});
	}
}
function add(SHOPPINGCARD_GOODS_ID, SHOPPINGCARD_GOODS_CARDID, SHOPPINGCARD_GOODS_PRICE) {
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var totalNumber = $("#totalNumber" + SHOPPINGCARD_GOODS_ID).text();
	var totalNumber = parseInt(totalNumber) + 1;
	if (totalNumber < 1) {
		return;
	} else {
		var url = adminQueryUrl + shoppingCardService + '/updateNumber.action';
		var param = 'todo=add&SHOPPINGCARD_GOODS_CARDID='
			+ SHOPPINGCARD_GOODS_CARDID + '&SHOPPINGCARD_GOODS_ID='
			+ SHOPPINGCARD_GOODS_ID + '&SHOPPINGCARD_GOODS_PRICE=' + SHOPPINGCARD_GOODS_PRICE;
		$.ajax({
			url : url,
			type : 'post',
			dataType : 'JSON',
			beforeSend : function(xhr) { //设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers : {
				'token' : token,
				'userId' : userId
			},
			data : param,
			success : function(data) {
				if (data.success == "1") {
					$("#totalNumber" + SHOPPINGCARD_GOODS_ID).text(totalNumber);
					var totalPrice = $("#totalPrice").html();
					var price = parseInt(totalPrice) + SHOPPINGCARD_GOODS_PRICE;
					$("#totalPrice").html(price);

				} else {
					new $.zui.Messager('系统繁忙,请稍候再试!', {
						type : 'warning',
						placement : 'center'
					}).show();
				}
			},
			error : function(e) {
				new $.zui.Messager('系统繁忙,请稍候再试!', {
					type : 'warning',
					placement : 'center'
				}).show();
			}
		});
	}
}

function deleteAll() {
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var cardId=$("#cardId").val();
	var url = adminQueryUrl + shoppingCardService + '/deleteAll.action';
	var param = 'cardId='+cardId;
	$.ajax({
		url : url,
		type : 'post',
		dataType : 'JSON',
		beforeSend : function(xhr) { //设置请求头信息
			xhr.setRequestHeader("token", token);
			xhr.setRequestHeader("userId", userId);
		},
		headers : {
			'token' : token,
			'userId' : userId
		},
		data : param,
		success : function(data) {

			if (data.success == "1") {
				$('#cardShow').empty();
				$("#nullDataText").show();
			} else {
				new $.zui.Messager('系统繁忙,请稍候再试!', {
					type : 'warning',
					placement : 'center'
				}).show();
			}
		},
		error : function(e) {
			new $.zui.Messager('系统繁忙,请稍候再试!', {
				type : 'warning',
				placement : 'center'
			}).show();
		}
	});

	
}

function submitShopCar(){
	$("#shopCardPanel").hide();
	$("#orderPanel").show();
	
	var outTradeNo = ""; //订单号
	for (var i = 0; i < 6; i++) //6位随机数，用以加在时间戳后面。
	{
		outTradeNo += Math.floor(Math.random() * 10);
	}
	outTradeNo = new Date().getTime() + outTradeNo; //时间戳，用来生成订单号。
	$("#orderNo").html(outTradeNo);
	$("#orderNoValue").val(outTradeNo);
	
	$("#orderPersonName").html($.zui.store.get("userId"));
	
	
}

