$(document).ready(function(){
	var currentPage = 0;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	//获取用户状态下拉框
	var queryPageListFun = function(){
		var url = adminQueryUrl + clientScore +queryPageListMethod;
		console.log('url2='+url+",toke="+token+",userId="+userId);
		var param ="pageSize="+pageSize;//请求到列表页面
		if ($("#queryClientName").val()!=''){
			param += "&queryClientName="+$("#queryClientName").val();
		}
		if ($("#queryClientTrumName").val()!=''){
			param += "&queryClientTrumName="+$("#queryClientTrumName").val();
		}
		
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param +="&currentPage=" +currentPage;
		param += "&UserId="+userId;
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data2='+data);
				$('#listDataGrid').empty();
				$('#listDataGrid').data('zui.datagrid',null);
				$('#listPager').empty();
				$('#listPager').data('zui.pager',null);
				if (data.success == '1'){
					if (data.pageTotal > 0){
						$('#listDataGrid').datagrid({
							checkable:true,
					        checkByClickRow: true,
						    dataSource: {
						        cols:[
						        	{name: 'PerIntegral_ID', label: '1', width: 1},
                                    {name: 'PerInf_Code', label: '用户名', width: 80},
                                    {name: 'PerUser_Code', label: '姓名', width: 80},
                                    {name: 'Integral', label: '电话号码', width: 80},
          
                                     {name: 'oprDate', label: '操作时间', width: -1}//-1用于自适应宽度
  						           
						        ],
						        cache:false,
						        array:data.pageList
						    }
						});
						//获取分页
						$('#listPager').pager({
						    page: data.currentPage,
						    recTotal: data.pageTotal,
						    recPerPage:pageSize,
						    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
						    pageSizeOptions:[10,20,30,50,100]
						});
					}
					else {
						$("#listDataGrid").html("<center>查无数据</center>");
					}
				}
				else {
					$("#listDataGrid").html("查无数据");
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	};
	queryPageListFun();
	//监听分页，修改当前页，条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		console.log('currentPage='+currentPage+",pageSize="+pageSize);
		queryPageListFun();
	});
	//搜索
	$("#searchBtn").click(function(){
		queryPageListFun();
	});
 
});