//相当于js的onload事件
$(document).ready(function(){
	
	var token = $.zui.store.get('token');
	var userId = $.zui.store.get('userId');
	console.log('userId='+userId);
	console.log('token='+token);
	
	$("#chooseBtn").click(function(){
		var roleType = $('input:radio:checked').val();
		if(roleType==0){
			location.href = adminOperationManager+"/login.jsp";
		}
		else if(roleType==1){
			location.href = adminAgentManager+"/login.jsp";
		}
		else if(roleType==2){
			location.href = adminMerchantManager+"/login.jsp";
		}
	});
	
	//监听按钮的事件 
	$("#managerLoginBtn").click(function(){
		$('#choseRoleModal').modal('show', 'fit');
	});
	
	//普通用户登录 
	$("#userLoginBtn").click(function(){
		//判断用户名是否为空
		if($("#loginName").val() == ''){
			$('#loginName').tooltip('show', '用户名不能为空!');
			return ;
		}
		if($("#passWd").val() == ''){
			$('#passWd').tooltip('show', '密码不能为空!');
			return ;
		}
		var loginUrl = adminQueryUrl+adminUserService + loginMethod;;
		var param = "username="+$("#loginName").val();
		param += "&upwd="+$("#passWd").val();
		console.log('loginUrl='+loginUrl);
		console.log('param='+param);
		$.ajax({
			url:loginUrl,
			type:'post',
			data:param,
			dataType:'JSON',
			success:function(result){
				//将字符串转成JSON对象
				//var jsonObj = jQuery.parseJSON(result);
				//alert(jsonObj.success);
				if(result.success == '1'){
					//保存服务器返回值到本地中
					$.zui.store.set('token',result.token);
					$.zui.store.set('userId', result.userId);
					$.zui.store.set('userName', result.userName); 
					$.zui.store.set("trueName", result.trueName);
					location.href = "admin/admin.jsp";
				}
				else{
					new $.zui.Messager('用户名或者密码错误!', {
						 type: 'warning',//定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍后再试!', {
					 type: 'warning',//定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});
	});
	
	//初始化注册中的下拉框
	$("#registeBtn").click(function(){
		//获取菜单列表
		$('#addModal').modal('show', 'fit');
		//重置表单
		document.getElementById("registerForm").reset();
		var initUrl = adminQueryUrl + adminUserService + initAddMethod;
		var param = "";	//请求到列表页面
		console.log("initUrl"+initUrl+param);
		$.ajax({
			url:initUrl,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success=='1'){
					//重置省份下拉框
					$("#addProvince").empty();
					var value = "";
					$("#addProvince").html("<option value="+value+">省</option>");
					//重置城市下拉框
					$("#addCity").empty();
					$("#addCity").html("<option value="+value+">市</option>");
					//重置市区下拉框
					$("#addRegion").empty();
					$("#addRegion").html("<option value="+value+">区</option>");
					//自动生成编码
					$("#addOrgCode").val(data.randomCode);
					
					$.each(data.provinceList, function(i, item){    
						$("#addProvince").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
					});
					//初始化机构下拉框
					$("#addUserOrgCode").empty();
					$("#addUserOrgCode").html("<option value="+value+">请选择</option>");
					$.each(data.orgList, function(i, item){     
						$("#addUserOrgCode").append("<option value='"+item.Org_Code+"'>"+item.Org_Name+"</option>");
					}); 
					//初始化性别下拉框
					$("#addUserSex").empty();
					$("#addUserSex").html("<option value="+value+">请选择</option>");
					$.each(data.sexList, function(i, item){     
						$("#addUserSex").append("<option value='"+item.KEY_VALUE+"'>"+item.KEY_NAME+"</option>");
					}); 
					//初始化角色下拉框
					$("#addUserRoleCode").empty();
					$("#addUserRoleCode").html("<option value="+value+">请选择</option>");
					$.each(data.roleList, function(i, item){     
						$("#addUserRoleCode").append("<option value='"+item.Role_ID+"'>"+item.RoleName+"</option>");
					}); 
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	
	//根所选省份获取对应的城市
		var provinceSel = document.getElementById("addProvince");
		provinceSel.onchange=function(){
				var provinceCode = provinceSel.options[provinceSel.selectedIndex].value;
				showCity(provinceCode);
		}
		//根所选城市获取对应的市区
		var CitySel = document.getElementById("addCity");
		CitySel.onchange=function(){
				var CityCode = CitySel.options[CitySel.selectedIndex].value;
				showRegion(CityCode);
		}

	//根据所选城市显示对应的区
	function showRegion(CityCode){
		var initRegionUrl = adminQueryUrl + institutionService + initAddMethod;
		var param = "&cityCode="+CityCode;	//请求到列表页面
		console.log("initUrl"+initRegionUrl+"param="+param);
		$.ajax({
			url:initRegionUrl,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success=='1'){
				$("#addRegion").empty();
				var value = "";
				$("#addRegion").html("<option value="+value+">区</option>");
					$.each(data.regionList, function(i, item){     
						$("#addRegion").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
					});
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	}
	
	//根据所选省份获取对应的城市
		function showCity(ProviceCode){
			var initCityUrl = adminQueryUrl + institutionService + initAddMethod;
			var param = "&provinceCode="+ProviceCode;	//请求到列表页面
			console.log("initUrl"+initCityUrl+"param="+param);
			$.ajax({
				url:initCityUrl,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token", token);
					xhr.setRequestHeader("userId", userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$("#addCity").empty();
						var value = "";
						$("#addCity").html("<option value="+value+">市</option>");
						
						$("#addRegion").empty();
						$("#addRegion").html("<option value="+value+">区</option>");
						$.each(data.cityList, function(i, item){     
							$("#addCity").append("<option value='"+item.Area_Code+"'>"+item.Area_Name+"</option>");
						});
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	
	//手机号验证
	jQuery.validator.addMethod("isMobile",function(value,element){
		if (value.length!=11){
			return false;
		}
		return true;
	},"手机号应为11位");
	
	//保存新增用户的信息
	$("#addSaveBtn").click(function(){
		var validLoginNameUrl = adminQueryUrl + adminUserService + "/validLoginName.action";
		console.log("validLoginNameUrl"+validLoginNameUrl);
		//表单验证
		$("#registerForm").validate({
			rules:{
				addLoginName:{
					"required":true,
					"remote":{
						url:validLoginNameUrl,
						type:'get',
						dataType:'json',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token", token);
							xhr.setRequestHeader("userId", userId);
						},
						headers:{'token':token,'userId':userId},
						data:{
							'addAreaCode':function(){
								//alert('$("#addMenuName").val()='+$("#addMenuName").val())
								return $("#addAreaCode").val();
							}
						}
					}
				},
				addTrueName:{
					"required":true
				},
				addUserOrgCode:{
					"required":true
				},
				addProvince:{
					"required":true
				},
				addUserRoleCode:{
					"required":true
				},
				addUserSex:{
					"required":true
				},
				addMobile:{
					"required":true,
					"isMobile":true
				},
				addEmail:{
					"email":true,
				}
			},
			messages:{
				addLoginName:{
					"required":"请输入用户名",
					"remote":"该用户名已经存在"
				},
				addTrueName:{
					"required":"请输入姓名"
				},
				addUserOrgCode:{
					"required":"请选择机构"
				},
				addProvince:{
					"required":"请选择省份,市区视情况选择"
				},
				addUserRoleCode:{
					"required":"请选择角色"
				},
				addUserSex:{
					"required":"请选择性别"
				},
				addMobile:{
					"required":"请填写手机号"
				}
			},
			//表单数据提交
			submitHandler:function(form){
				//form.submit();
				var addUrl = adminQueryUrl + adminUserService + "/add.action";
				console.log("userAddUrl"+addUrl);
				$.ajax({
					url:addUrl,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token", token);
						xhr.setRequestHeader("userId", userId);
					},
					headers:{'token':token,'userId':userId},
					//serialize()表单数据序列化
					data:$(form).serialize(),
					success:function(data){
						console.log('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('注册成功，初始密码为123456。', {
						    type: 'success',
						    placement:'center'
						}).show();
		                	$('#addModal').modal('hide', 'fit');
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});
	});
		
});