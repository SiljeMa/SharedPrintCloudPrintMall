$(document).ready(function(){
	var currentPage = 0;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	//获取用户状态下拉框
	var queryOrderStateFun = function(){
		
	
		var url = adminQueryUrl + dicsService + queryPageListMethod;
		console.log('url2='+url+",toke="+token+",userId="+userId);
		var param = '';
		param += "dicsCode=CHECK_STATUS";
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(result){
				
				if (result.dicsList!=null && result.dicsList.length>0){
				
					for (var i=0;i<result.dicsList.length;i++){
						$("#queryOrderState").append("<option value='"+result.dicsList[i].KEY_VALUE+"'>"+result.dicsList[i].KEY_NAME+"</option>");	
					}
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	};
	queryOrderStateFun();
	var queryPageListFun = function(){
		var url = adminQueryUrl + order +queryPageListMethod;
		console.log('url2='+url+",toke="+token+",userId="+userId);
		var param ="pageSize="+pageSize;//请求到列表页面
		if ($("#queryOrderNo").val()!=''){
			param += "&queryOrderNo="+$("#queryOrderNo").val();
		}
		if ($("#queryOrderState").val()!=''){
			param += "&queryOrderState="+$("#queryOrderState").val();
		}
		if ($("#queryOrderCreateDate").val()!=''){
			param += "&queryOrderCreateDate="+$("#queryOrderCreateDate").val();
		}
		if ($("#queryOrderEndDate").val()!=''){
			param += "&queryOrderEndDate="+$("#queryOrderEndDate").val();
		}
		
		
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param +="&currentPage=" +currentPage;
		param += "&CloudId="+userId;
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data2='+data);
				$('#listDataGrid').empty();
				$('#listDataGrid').data('zui.datagrid',null);
				$('#listPager').empty();
				$('#listPager').data('zui.pager',null);
				if (data.success == '1'){
					if (data.pageTotal > 0){
						$('#listDataGrid').datagrid({
							checkable:true,
					        checkByClickRow: true,
						    dataSource: {
						        cols:[
						        	{name: 'ORDER_ID', label: '1', width: 1},
                                    {name: 'ORDER_NO', label: '订单号', width: 80},
                                    {name: 'orderName', label: '订单状态', width: 100},
                                    {name: 'BUYER_ID', label: '下单人编号', width: 100},
                                    {name: 'ORDER_PRICE', label: '订单金额', width: 80},
                                    {name: 'oprDate', label: '订单创建时间', width: -1}//-1用于自适应宽度
  						           
						        ],
						        cache:false,
						        array:data.pageList
						    }
						});
						//获取分页
						$('#listPager').pager({
						    page: data.currentPage,
						    recTotal: data.pageTotal,
						    recPerPage:pageSize,
						    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
						    pageSizeOptions:[10,20,30,50,100]
						});
					}
					else {
						$("#listDataGrid").html("<center>查无数据</center>");
					}
				}
				else {
					$("#listDataGrid").html("查无数据");
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	};
	queryPageListFun();
	//监听分页，修改当前页，条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		console.log('currentPage='+currentPage+",pageSize="+pageSize);
		queryPageListFun();
	});
	//搜索
	$("#searchBtn").click(function(){
		queryPageListFun();
	});
	$("#detailBtn").click(function(){
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
		url=adminQueryUrl + order  +detailMethod;
		var param = 'ORDER_NO='+selectedItems[0].ORDER_NO;
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				
				$('#detailModal').modal('show', 'fit');
				$("#detailORDER_NO").html(data.searchMap1.ORDER_NO);
				$("#detailORDER_STATE").html(data.searchMap1.orderName); 
				$("#detailBUYER_ID").html(data.searchMap1.BUYER_ID);
				$("#detailORDER_PRICE").html(data.searchMap1.ORDER_PRICE);
				$("#detailORDER_CREATEDATE").html(data.searchMap1.oRDER_CREATEDATE);
				$("#detailORDERCHANGEDATE").html(data.searchMap1.oRDERCHANGEDATE);
				$("#detailREFUNDA_MOUNT").html(data.searchMap1.REFUNDA_MOUNT);
				$("#detailREFUND_DATE").html(data.searchMap1.rEFUND_DATE);
				$("#detailORDERRE_MARK").html(data.searchMap1.ORDERRE_MARK);
				$("#detailASSESSOR").html(data.searchMap1.ASSESSOR);
				$("#detailAFFILIATED_organization").html(data.searchMap1.org_Name);
				$("#detailOPINION").html(data.searchMap1.OPINION);
				$("#detailAFFILIATED_MERCHANT").html(data.searchMap1.merUserName);
				$("#detailAFFILIATED_agent").html(data.searchMap1.ageTrueName);
				
				$("#detailCOMMODITY_NAME").html(data.searchMap2.COMMODITY_NAME);
				$("#detailCOMMODITY_COUNT").html(data.searchMap2.COMMODITY_COUNT);
				$("#detailCOMMODITY_CPRICE").html(data.searchMap2.COMMODITY_CPRICE);
				$("#detailCOMMODITY_TOTALCPRICE").html(data.searchMap2.COMMODITY_TOTALCPRICE);
				$("#detailCOMMODITY_CONTENT").html(data.searchMap2.COMMODITY_CONTENT);
				$("#detailPRODUCT_CATEGORY_NAME").html(data.searchMap2.PRODUCT_CATEGORY_NAME);
				$("#detailBRAND_NAME").html(data.searchMap2.BRAND_NAME);
				$("#detailOPRDATE2").html(data.searchMap2.oPRDATE);
				
				$("#detailCONSIGEE").html(data.searchMap3.CONSIGEE);
				$("#detailTELEPHONE").html(data.searchMap3.TELEPHONE);
				$("#detailADDRESS").html(data.searchMap3.ADDRESS);
				$("#detailOPRDATE1").html(data.searchMap3.oPRDATE);
				var headpic1 = data.searchMap2.PICTURE1;
				if (headpic1!=null && headpic1!='' && typeof(headpic1)!=undefined){
					var imgSrc = adminQueryUrl + "/" + headpic1;
					var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
					$("#detailPICTURE1").html(imgHtml);
				}
				var headpic2 = data.searchMap2.PICTURE2;
				if (headpic2!=null && headpic2!='' && typeof(headpic2)!=undefined){
					var imgSrc = adminQueryUrl + "/" + headpic2;
					var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
					$("#detailPICTURE2").html(imgHtml);
				}
				var headpic3 = data.searchMap2.PICTURE3;
				if (headpic3!=null && headpic3!='' && typeof(headpic3)!=undefined){
					var imgSrc = adminQueryUrl + "/" + headpic3;
					var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
					$("#detailPICTURE3").html(imgHtml);
				}
				var headpic4 = data.searchMap2.PICTURE4;
				if (headpic4!=null && headpic4!='' && typeof(headpic4)!=undefined){
					var imgSrc = adminQueryUrl + "/" + headpic4;
					var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
					$("#detailPICTURE4").html(imgHtml);
				}
				var headpic5 = data.searchMap2.PICTURE5;
				if (headpic5!=null && headpic5!='' && typeof(headpic5)!=undefined){
					var imgSrc = adminQueryUrl + "/" + headpic5;
					var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
					$("#detailPICTURE5").html(imgHtml);
				}
				var headpic6 = data.searchMap2.PICTURE6;
				if (headpic6!=null && headpic6!='' && typeof(headpic6)!=undefined){
					var imgSrc = adminQueryUrl + "/" + headpic6;
					var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
					$("#detailPICTURE6").html(imgHtml);
				}
			},
			error:function(e){
				new $.zui.Messager('请上传参数', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
		}
	});
});