$(document).ready(function(){
	var currentPage = 0;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var queryPageListFun = function(){
		var url = adminQueryUrl + file +queryPageListMethod;
		console.log('url2='+url+",toke="+token+",userId="+userId);
		var param = "&pageSize="+pageSize;;//请求到列表页面
		if ($("#queryFileName").val()!=''){
			param += "&queryFileName="+$("#queryFileName").val()
		}
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param +="&currentPage=" +currentPage;
		param += "&excludeUserId="+userId;
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data2='+data);
				$('#listDataGrid').empty();
				$('#listDataGrid').data('zui.datagrid',null);
				$('#listPager').empty();
				$('#listPager').data('zui.pager',null);
				if (data.success == '1'){
					if (data.pageTotal > 0){
						$('#listDataGrid').datagrid({
							checkable:true,
					        checkByClickRow: true,
						    dataSource: {
						        cols:[
						        	{name: 'PerFile_ID', label: '1', width: 1},
						            {name: 'FileName', label: '文件名称', width: 134},
						            {name: 'FilePath', label: '文件路径', width: 109},
						            {name: 'FileType', label: '文件扩展名', width: 109},
						            {name: 'Remark', label: '文件备注', width: 109},
						            {name: 'opr_time', label: '操作时间', width: -1},
						           
						        ],
						        cache:false,
						        array:data.pageList
						    }
						});
						//获取分页
						$('#listPager').pager({
						    page: data.currentPage,
						    recTotal: data.pageTotal,
						    recPerPage:pageSize,
						    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
						    pageSizeOptions:[10,20,30,50,100]
						});
					}
					else {
						$("#listDataGrid").html("<center>查无数据</center>");
					}
				}
				else {
					$("#listDataGrid").html("查无数据");
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	};
	queryPageListFun();
	//监听分页，修改当前页，条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		console.log('currentPage='+currentPage+",pageSize="+pageSize);
		queryPageListFun();
	});
	//搜索
	$("#searchBtn").click(function(){
		queryPageListFun();
	});
	//初始化添加
	var tempFile;
	$("#addBtn").click(function(){
		$("#addForm")[0].reset();
		if (tempFile!=null){
			$('#uploadHeadpic').data('zui.uploader').removeFile(tempFile);
			tempFile = null;
		}
	});
	$('#uploadHeadpic').uploader({
	    autoUpload: true,            // 当选择文件后立即自动进行上传操作
	    url: adminQueryUrl + fileService + uploadUserHeadpic,  // 文件上传提交地址
	    limitFilesCount:1,
	    responseHandler: function(responseObject, file) {
	    	tempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		console.log('resultJson='+resultJson);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#addHeadpic").val(resultJson.fileName);
	    				$("#addHeadpic2").val(resultJson.filePath);
	    				$("#addHeadpic3").val(resultJson.filelatter);
	    				console.log(resultJson.fileName);
	    				console.log(resultJson.filePath);
	    				console.log(resultJson.filelatter);
	    			}
	    		}
	    	}
	    }
	});
	$("#addSave").click(function(){
		$("#addForm").validate({
			rules:{
				addRemark:{
					"required":true
				}
			},
			submitHandler:function(form){
				if($("#addHeadpic").val()==''){
					new $.zui.Messager('请上传大头像!', {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
					return;
				}
				
				$.ajax({
					url:adminQueryUrl + file + addMethod,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					headers:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						console.log('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('添加成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#addModal').modal('hide', 'fit');
		                	queryPageListFun();
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});
	});
	

	$("#deleteBtn").click(function(){

		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$('#deleteModal').modal('show', 'fit');
		}
	});
	$("#deleteFileBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		var url = adminQueryUrl + file + deleteMethod;
		var param = '&PerFile_ID='+selectedItems[0].PerFile_ID;//请求到列表页面
		console.log('param2='+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				$('#deleteModal').modal('hide', 'fit');
				if (data.success=='1'){
	                	new $.zui.Messager('删除成功!', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	queryPageListFun();
				}
                else{
                	new $.zui.Messager(data.msg, {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	$("#detailBtn").click(function(){
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {

			$("#detailForm")[0].reset();
			var url = adminQueryUrl + file +detailMethod;
			var param = '&PerFile_ID='+selectedItems[0].PerFile_ID;//请求到列表页面
			console.log('param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#detailModal').modal('show', 'fit');
						$("#detailPerUser_Code").html(data.map.PerUser_Code);
						$("#detailFileName").html(data.map.FileName);
						$("#detailFilePath").html(data.map.FilePath);
						$("#detailFileType").html(data.map.FileType);
						$("#detailRemark").html(data.map.Remark);
						$("#detailOpr_id").html(data.map.Opr_id);
						$("#detailOpr_time").html(data.map.opr_time);
						
						
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		
			
		}
	});
});